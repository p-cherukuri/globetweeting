import { GlobetweetingPage } from './app.po';

describe('globetweeting App', () => {
  let page: GlobetweetingPage;

  beforeEach(() => {
    page = new GlobetweetingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
