webpackJsonp([1,4],{

/***/ 1018:
/***/ (function(module, exports) {

module.exports = "<style>\n.open{\n\tposition: fixed;\n\ttop: 40px;\n\tright: 40px;\n\twidth: 50px;\n\theight: 50px;\n\tdisplay: block;\n\tcursor: pointer;\n\ttransition: opacity 0.2s linear;\n\t&:hover{\n\t\topacity: 0.8;\n\t}\n\tspan{\n\t\tdisplay: block;\n\t\tfloat: left;\n\t\tclear: both;\n\t\theight: 4px;\n\t\twidth: 40px;\n\t\tborder-radius: 40px;\n\t\tbackground-color: #fff;\n\t\tposition: absolute;\n\t\tright: 3px;\n\t\ttop: 3px;\n\t\toverflow: hidden;\n\t\ttransition: all 0.4s ease;\n\t\t&:nth-child(1){\n\t\t\tmargin-top: 10px;\n\t\t\tz-index: 9;\n\t\t}\n\t\t&:nth-child(2){\n\t\t\tmargin-top: 25px;\n\t\t}\n\t\t&:nth-child(3){\n\t\t\tmargin-top: 40px;\n\t\t}\n\t}\n}\n.sub-menu{\n\ttransition: all 0.8s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n\t\theight: 0;\n\t\twidth: 0;\n\t\tright: 0;\n\t\ttop: 0;\n\t\tposition: absolute;\n\t\tbackground-color: rgba(38, 84, 133, 0.54);\n\t\tborder-radius: 50%;\n\t\tz-index: 18;\n\t\toverflow: hidden;\n\t\tli{\n\t\t\tdisplay: block;\n\t\t\tfloat: right;\n\t\t\tclear: both;\n\t\t\theight: auto;\n\t\t\tmargin-right: -160px;\n\t\t\ttransition: all 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n\t\t\t&:first-child{\n\t\t\t\tmargin-top: 180px;\n\t\t\t}\n\t\t\t&:nth-child(1){\n\t\t\t\t-webkit-transition-delay: 0.05s;\n\t\t\t}\n\t\t\t&:nth-child(2){\n\t\t\t\t-webkit-transition-delay: 0.10s;\n\t\t\t}\n\t\t\t&:nth-child(3){\n\t\t\t\t-webkit-transition-delay: 0.15s;\n\t\t\t}\n\t\t\t&:nth-child(4){\n\t\t\t\t-webkit-transition-delay: 0.20s;\n\t\t\t}\n\t\t\t&:nth-child(5){\n\t\t\t\t-webkit-transition-delay: 0.25s;\n\t\t\t}\n\t\t\ta{\n\t\t\t\tcolor: #fff;\n\t\t\t\tfont-family: 'Lato', Arial, Helvetica, sans-serif;\n\t\t\t\tfont-size: 16px;\n\t\t\t\twidth: 100%;\n\t\t\t\tdisplay: block;\n\t\t\t\tfloat: left;\n\t\t\t\tline-height: 40px;\n\t\t\t}\n\t\t}\n\t}\n\n\t.oppenned{\n\t\t.sub-menu{\n\t\t\topacity: 1;\n\t\t\theight: 400px;\n\t\t\twidth: 400px;\n\t\t}\n\t\tspan:nth-child(2){\n\t\t\toverflow: visible;\n\t\t}\n\t\tspan:nth-child(1), span:nth-child(3){\n\t\t\tz-index: 100;\n\t\t\ttransform: rotate(45deg);\n\t\t}\n\t\tspan:nth-child(1){\n\t\t\ttransform: rotate(45deg) translateY(12px) translateX(12px);\n\t\t}\n\t\tspan:nth-child(2){\n\t\t\theight: 400px;\n\t\t\twidth: 400px;\n\t\t\tright: -160px;\n\t\t\ttop: -160px;\n\t\t\tborder-radius: 50%;\n\t\t\tbackground-color: rgba(38, 84, 133, 0.54);\n\t\t}\n\t\tspan:nth-child(3){\n\t\t\ttransform: rotate(-45deg) translateY(-10px) translateX(10px);\n\t\t}\n\t\tli{\n\t\t\tmargin-right: 168px;\n\t\t}\n\t}\n.button{\n\tdisplay: block;\n\tfloat: left;\n  clear: both;\n\tpadding: 20px 40px;\n\tbackground: #fff;\n\tborder-radius: 3px;\n\tborder: 2px solid #10a1ea;\n\toverflow: hidden;\n\tposition: relative;\n\t&:after{\n\t\ttransition: transform 0.3s ease;\n\t\tcontent: \"\";\n\t\tposition: absolute;\n\t\theight: 200px;\n\t\twidth: 400px;\n\t\ttransform: rotate(45deg) translateX(-540px) translateY(-100px);\n\t\tbackground: #10a1ea;\n\t\tz-index: 1;\n\t}\n\t&:before{\n\t\ttransition: transform 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n\t\tcontent: attr(title);\n\t\tposition: absolute;\n\t\ttop: 0;\n\t\tleft: 0;\n\t\twidth: 100%;\n\t\theight: 100%;\n\t\tcolor: #fff;\n\t\tz-index: 2;\n\t\ttext-align: center;\n\t\tpadding: 20px 40px;\n\t\ttransform: translateY(200px);\n\t}\n\t&:hover{\n\t\ttext-decoration: none;\n\t\t&:after{\n\t\t\ttransform: translateX(-300px) translateY(-100px);\n\t\t}\n\t\t&:before{\n\t\t\ttransform: translateY(0);\n\t\t}\n\t}\n}\n\n.contact-icons {\n\t-webkit-flex: 1; /* Safari 6.1+ */\n    -ms-flex: 1; /* IE 10 */\n    flex: 1;\n\t\t-webkit-flex-direction: row;\n    flex-direction: row;\n}\n\n.github {\n}\n\n.linkedin {\n\tleft:10%;\n}\n\n.email {\n\tleft:10%;\n}\n\n</style>\n\n<div layout=\"column\" layout-fill>\n  <md-toolbar color=\"primary\" class=\"md-whiteframe-z1\">\n    <span>\n\n      <!--<a md-icon-button mdTooltip=\"Docs\" href=\"https://teradata.github.io/covalent/\" target=\"_blank\"><md-icon>chrome_reader_mode</md-icon></a>-->\n      <button md-icon-button mdTooltip=\"Home\" (click)=\"goBack()\" target=\"_blank\"><md-icon class=\"md-30\" color=\"accent\">arrow_back</md-icon></button>\n    </span>\n    <span>About</span>\n\n  </md-toolbar>\n  <md-tab-group md-stretch-tabs color=\"accent\">\n  <md-tab>\n    <ng-template md-tab-label>What is this?</ng-template>\n\t\t<div class=\"description\" align=\"center\">\n\t\t\t<h3 align=\"center\">Globetweeting.com is a website that shows live tweets around the world about top trending Twitter topics in the USA.</h3>\n\t\t\t<h4 align=\"center\">- You can pick and choose what trends you'd like to see tweets about using the left menu on the home page. Trends are updated approximately every 5 minutes.</h4>\n\t\t\t<h4 align=\"center\">- You can pause the globe using the button in the top right corner and click on tweet info to be redirected to that Twitter user's profile.</h4>\n\t\t</div>\n  </md-tab>\n  <md-tab>\n    <ng-template md-tab-label>Creator</ng-template>\n\t\t<div class=\"creator-info\">\n\t\t\t<h1 align=\"center\"><b>Phani Cherukuri</b></h1>\n\t\t\t<div class=\"contact-icons\" align=\"center\">\n\t\t\t\t<a class=\"github\" href=\"https://github.com/p-cherukuri\"><img src=\"assets/icons/github-icon.png\" width=\"35\" height=\"35\"></a>\n\t\t\t\t<a class=\"linkedin\" href=\"https://www.linkedin.com/in/phanindra-cherukuri\"><img src=\"assets/icons/linkedin-icon.png\" width=\"40\" height=\"30\"></a>\n\t\t\t\t<!--<a class=\"email\" onclick=\"window.open('mailto:phani.cheruk@gmail.com?subject=Globetweeting&nbsp;Inquiry');\"><img src=\"assets/icons/email-icon.png\" width=\"40\" height=\"40\"></a>-->\n\t\t\t</div>\n\t\t\t<div>\n\t\t\t\t<h4 align=\"center\"><b>Questions or feedback? Shoot me an email:</b></h4>\n\t\t\t\t<div>\n\t\t\t\t\t<form align=\"center\" method=\"POST\" action=\"http://formspree.io/phani.cheruk@gmail.com\">\n\t\t\t\t\t\t<md-input-container>\n\t\t\t\t\t\t\t<input mdInput type=\"email\" name=\"email\" placeholder=\"Your email\">\n\t\t\t\t\t\t</md-input-container>\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t<md-input-container>\n\t\t\t\t\t\t  \t<textarea mdInput name=\"message\" placeholder=\"Your message\"></textarea>\n\t\t\t\t\t\t  </md-input-container>\n\t\t\t\t\t\t</p>\n\n\t\t\t\t\t  <button md-raised-button color=\"primary\" type=\"submit\" value=\"Send\">Send email</button>\n\t\t\t\t\t</form>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n  </md-tab>\n</md-tab-group>\n</div>\n"

/***/ }),

/***/ 1019:
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ 1020:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav logo=\"assets:covalent\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n    <span>Quickstart</span>\n    <span flex></span>\n    <a md-icon-button mdTooltip=\"Docs\" href=\"https://teradata.github.io/covalent/\" target=\"_blank\"><md-icon>chrome_reader_mode</md-icon></a>\n    <a md-icon-button mdTooltip=\"Github\" href=\"https://github.com/teradata/covalent\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <td-layout-manage-list #manageList\n                        [opened]=\"media.registerQuery('gt-sm') | async\"\n                        [mode]=\"(media.registerQuery('gt-sm') | async) ? 'side' : 'push'\"\n                        [sidenavWidth]=\"(media.registerQuery('gt-xs') | async) ? '257px' : '100%'\">\n    <md-toolbar td-sidenav-content>\n      <span>Product Name</span>\n    </md-toolbar>\n    <md-nav-list td-sidenav-content>\n      <a md-list-item\n        md-ripple class=\"block relative\"\n        (click)=\"!media.query('gt-sm') && manageList.close()\"\n        [routerLinkActive]=\"['active']\"\n        [routerLinkActiveOptions]=\"{exact:true}\"\n        [routerLink]=\"['/product']\">\n        <md-icon md-list-icon>dashboard</md-icon>\n        Dashboard\n      </a>\n      <a md-list-item\n        md-ripple class=\"block relative\"\n        (click)=\"!media.query('gt-sm') && manageList.close()\"\n        [routerLinkActive]=\"['active']\"\n        [routerLinkActiveOptions]=\"{exact:true}\"\n        [routerLink]=\"['stats']\">\n        <md-icon md-list-icon>insert_chart</md-icon>\n        Stats\n      </a>\n      <a md-list-item\n        md-ripple class=\"block relative\"\n        (click)=\"!media.query('gt-sm') && manageList.close()\"\n        [routerLinkActive]=\"['active']\"\n        [routerLinkActiveOptions]=\"{exact:true}\"\n        [routerLink]=\"['features']\">\n        <md-icon md-list-icon>star</md-icon>\n        Features\n      </a>\n      <a md-list-item (click)=\"!media.query('gt-sm') && manageList.close()\" md-ripple class=\"block relative\">\n        <md-icon md-list-icon>people</md-icon>\n        Customers\n      </a>\n      <h3 md-subheader>Admin Menu</h3>\n      <a md-list-item (click)=\"!media.query('gt-sm') && manageList.close()\" md-ripple class=\"block relative\">\n        <md-icon md-list-icon>receipt</md-icon>\n        Log\n      </a>\n      <a md-list-item (click)=\"!media.query('gt-sm') && manageList.close()\" md-ripple class=\"block relative\">\n        <md-icon md-list-icon>settings</md-icon>\n        Settings\n      </a>\n    </md-nav-list>\n    <div td-toolbar-content layout=\"row\" layout-align=\"start center\" flex>\n      <span>{{title}}</span>\n      <span flex></span>\n      <button md-icon-button><md-icon class=\"md-24\">view_module</md-icon></button>\n      <button md-icon-button><md-icon class=\"md-24\">sort</md-icon></button>\n      <button md-icon-button><md-icon class=\"md-24\">settings</md-icon></button>\n      <button md-icon-button><md-icon class=\"md-24\">more_vert</md-icon></button>\n    </div>\n    <router-outlet></router-outlet>\n  </td-layout-manage-list>\n</td-layout-nav>\n"

/***/ }),

/***/ 1021:
/***/ (function(module, exports) {

module.exports = "<md-card tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['push']\">\n    <ng-template tdLoading=\"features.load\">\n        <td-search-box class=\"push-left push-right\" placeholder=\"search\" [alwaysVisible]=\"true\" (searchDebounce)=\"filterFeatures($event)\"></td-search-box>\n        <md-divider></md-divider>\n        <md-list class=\"will-load\">\n            <div class=\"md-padding\" *ngIf=\"!filteredFeatures || filteredFeatures.length === 0\" layout=\"row\" layout-align=\"center center\">\n                <h3>No feature to display.</h3>\n            </div>\n            <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"filteredFeatures\">\n                <md-list-item>\n                    <md-slide-toggle md-list-icon [(ngModel)]=\"item.enabled\"></md-slide-toggle>\n                    <h3 md-line> {{item.title}} </h3>\n                    <span flex></span>\n                    <span flex-gt-xs=\"60\" flex-xs=\"40\" layout-gt-xs=\"row\">\n                        <div class=\"md-caption tc-grey-500\" flex-gt-xs=\"50\"> {{ item.user }} </div>\n                        <div class=\"md-caption tc-grey-500\" flex-gt-xs=\"50\"> {{ item.modified | timeAgo }} </div>\n                    </span>\n                    <span>\n                        <button md-icon-button [md-menu-trigger-for]=\"menu\">\n                        <md-icon>more_vert</md-icon>\n                        </button>\n                        <md-menu x-position=\"before\" #menu=\"mdMenu\">\n                            <a [routerLink]=\"[item.id + '/edit']\" md-menu-item>Edit</a>\n                            <button (click)=\"openConfirm(item.id)\" md-menu-item>Delete</button>\n                        </md-menu>\n                    </span>\n                </md-list-item>\n                <md-divider *ngIf=\"!last\"></md-divider>\n            </ng-template>\n        </md-list>\n    </ng-template>\n</md-card>\n<a md-fab color=\"accent\" class=\"mat-fab-bottom-right fixed\" [routerLink]=\"['add']\"> \n    <md-icon>add</md-icon>\n</a>\n"

/***/ }),

/***/ 1022:
/***/ (function(module, exports) {

module.exports = "<md-card tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['push']\">\n  <md-card-title>\n    Feature Form\n  </md-card-title>\n  <md-divider></md-divider>\n  <md-card-content class=\"push-bottom-none\">\n    <form #featureForm=\"ngForm\">\n      <div layout=\"row\">\n        <md-input-container flex>\n          <input mdInput\n                  #titleElement\n                  #titleControl=\"ngModel\" \n                  type=\"text\" \n                  placeholder=\"Feature Title\" \n                  [(ngModel)]=\"title\" \n                  name=\"title\" \n                  maxlength=\"20\" \n                  required>\n          <md-hint align=\"start\">\n            <span [hidden]=\"titleControl.pristine || !titleControl.errors?.required\" class=\"tc-red-600\">Required</span>\n          </md-hint>\n          <md-hint align=\"end\">{{titleElement.value.length}} / 20</md-hint>\n        </md-input-container>\n      </div>\n      <div layout=\"row\" class=\"push-top\">\n        <md-input-container flex>\n          <input mdInput\n                  #userElement\n                  #userControl=\"ngModel\"\n                  type=\"text\" \n                  placeholder=\"User\" \n                  [(ngModel)]=\"user\" \n                  name=\"user\"\n                  maxlength=\"30\"\n                  required>\n          <md-hint align=\"start\">\n            <span [hidden]=\"userControl.pristine || !userControl.errors?.required\" class=\"tc-red-600\">Required</span>\n          </md-hint>\n          <md-hint align=\"end\">{{userElement.value.length}} / 30</md-hint>\n        </md-input-container>\n      </div>\n      <div layout=\"row\">\n        <md-slide-toggle [(ngModel)]=\"enabled\" name=\"enabled\">Enabled</md-slide-toggle>\n      </div>\n    </form>\n    </md-card-content>\n    <md-divider></md-divider>\n    <md-card-actions>\n      <button md-button [disabled]=\"!featureForm.valid\" color=\"primary\" (click)=\"save()\">SAVE</button>\n      <button md-button (click)=\"goBack()\">CANCEL</button>\n    </md-card-actions>\n  </md-card>"

/***/ }),

/***/ 1023:
/***/ (function(module, exports) {

module.exports = "<div class=\"md-content\" class=\"inset\">\n    <div layout-gt-xs=\"row\">\n        <div flex-gt-xs=\"50\">\n          <md-card>\n              <md-card-title>Compute Usage</md-card-title>\n              <md-divider></md-divider>\n              <div class=\"chart-height push-top-sm\">\n                <ngx-charts-line-chart\n                  [scheme]=\"orangeColorScheme\"\n                  [results]=\"multi\"\n                  [gradient]=\"gradient\"\n                  [xAxis]=\"showXAxis\"\n                  [yAxis]=\"showYAxis\"\n                  [legend]=\"showLegend\"\n                  [showXAxisLabel]=\"showXAxisLabel\"\n                  [showYAxisLabel]=\"showYAxisLabel\"\n                  [xAxisLabel]=\"xAxisLabel\"\n                  [yAxisLabel]=\"yAxisLabel\"\n                  [autoScale]=\"autoScale\"\n                  [yAxisTickFormatting]=\"axisDigits\">\n                </ngx-charts-line-chart>\n              </div>\n              <md-divider></md-divider>\n              <md-card-actions>\n                <a md-button color=\"accent\">\n                  <span class=\"text-upper\">View Stats</span>\n                </a>\n              </md-card-actions>\n          </md-card>\n        </div>\n        <div flex-gt-xs=\"50\">\n          <md-card>\n              <md-card-title>Ingest Usage</md-card-title>\n              <md-divider></md-divider>\n              <div class=\"chart-height push-top-sm\">\n                <ngx-charts-line-chart\n                  [scheme]=\"blueColorScheme\"\n                  [results]=\"multi2\"\n                  [gradient]=\"gradient\"\n                  [xAxis]=\"showXAxis\"\n                  [yAxis]=\"showYAxis\"\n                  [legend]=\"showLegend\"\n                  [showXAxisLabel]=\"showXAxisLabel\"\n                  [showYAxisLabel]=\"showYAxisLabel\"\n                  [xAxisLabel]=\"xAxisLabel\"\n                  [yAxisLabel]=\"yAxisLabel\"\n                  [autoScale]=\"autoScale\"\n                  [yAxisTickFormatting]=\"axisDigits\">\n                </ngx-charts-line-chart>\n              </div>\n              <md-divider></md-divider>\n              <md-card-actions>\n                <a md-button color=\"accent\">\n                  <span class=\"text-upper\">View Stats</span>\n                </a>\n              </md-card-actions>\n          </md-card>\n        </div>\n    </div>\n    <div layout-gt-sm=\"row\">\n        <div flex-gt-sm=\"60\">\n            <md-card>\n            <md-card-title>All Activity</md-card-title>\n            <md-card-subtitle>for this product</md-card-subtitle>\n            <md-divider></md-divider>\n            <md-nav-list class=\"will-load item-list\">\n                <ng-template tdLoading=\"items.load\">\n                <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"items\">\n                    <a md-list-item layout-align=\"row\" [routerLink]=\"['../item', item.item_id]\">\n                    <md-icon md-list-avatar> {{item.icon}} </md-icon>\n                    <h3 md-line> {{item.name}} </h3>\n                    <p md-line> {{item.description | truncate:40}} </p>\n                    <span flex></span>\n                    <span class=\"md-caption text-right\" flex=\"20\"> {{item.created | timeAgo }} </span>\n                    </a>\n                    <md-divider *ngIf=\"!last\" md-inset></md-divider>\n                </ng-template>\n                </ng-template>\n            </md-nav-list>\n            </md-card>\n        </div>\n        <div flex-gt-sm=\"40\">\n            <md-card>\n            <md-card-title>Customers</md-card-title>\n            <md-card-subtitle>for this product</md-card-subtitle>\n            <md-divider></md-divider>\n            <md-list class=\"will-load user-list\">\n                <ng-template tdLoading=\"users.load\">\n                <ng-template let-user let-last=\"last\" ngFor [ngForOf]=\"users\">\n                    <md-list-item layout-align=\"row\">\n                    <md-icon md-list-icon>account_box</md-icon>\n                    <h3 md-line> {{user.displayName}} </h3>\n                    <p md-line> {{user.id}} </p>\n                    </md-list-item>\n                    <md-divider *ngIf=\"!last\" md-inset></md-divider>\n                </ng-template>\n                </ng-template>\n            </md-list>\n            <md-divider></md-divider>\n            <md-card-actions>\n                <a md-button color=\"accent\" class=\"text-upper\" [routerLink]=\"['/users']\">\n                <span>View More</span>\n                </a>\n            </md-card-actions>\n            </md-card>\n        </div>\n    </div>\n</div>\n<a md-fab color=\"accent\" class=\"mat-fab-bottom-right fixed\" [routerLink]=\"['/form']\"> \n    <md-icon>add</md-icon>\n</a>\n"

/***/ }),

/***/ 1024:
/***/ (function(module, exports) {

module.exports = "<div class=\"md-content\" class=\"inset\">\n    <div layout=\"column\" layout-gt-xs=\"row\">\n        <div flex-gt-xs=\"100\">\n            <md-card>\n              <md-card-title>Product Usage</md-card-title>\n              <md-divider></md-divider>\n              <div class=\"chart-height push-top-sm pull-bottom\">\n                <ngx-charts-line-chart\n                  [scheme]=\"colorScheme\"\n                  [results]=\"multi\"\n                  [gradient]=\"gradient\"\n                  [xAxis]=\"showXAxis\"\n                  [yAxis]=\"showYAxis\"\n                  [legend]=\"showLegend\"\n                  [showXAxisLabel]=\"showXAxisLabel\"\n                  [showYAxisLabel]=\"showYAxisLabel\"\n                  [xAxisLabel]=\"xAxisLabel\"\n                  [yAxisLabel]=\"yAxisLabel\"\n                  [autoScale]=\"autoScale\"\n                  [yAxisTickFormatting]=\"axisDigits\">\n                </ngx-charts-line-chart>\n              </div>\n          </md-card>\n        </div>\n    </div>\n    <div layout=\"column\" layout-gt-sm=\"row\">\n        <div flex-gt-sm=\"100\">\n            <md-card>\n                <div layout=\"row\" layout-align=\"start center\" class=\"pad-left-sm pad-right-sm\">\n                    <span *ngIf=\"!searchBox.searchVisible\" class=\"push-left-sm\">\n                        <span class=\"md-title\">Product Stats</span>\n                    </span>\n                    <td-search-box #searchBox backIcon=\"arrow_back\" class=\"push-right-sm\" placeholder=\"Search here\" (searchDebounce)=\"search($event)\" flex>\n                    </td-search-box>\n                </div>\n                <md-divider></md-divider>\n                <td-data-table\n                    #dataTable\n                    [data]=\"filteredData\"\n                    [columns]=\"columns\"\n                    [sortable]=\"true\"\n                    [sortBy]=\"sortBy\"\n                    [sortOrder]=\"sortOrder\"\n                    (sortChange)=\"sort($event)\">\n                </td-data-table>\n                <div class=\"md-padding\" *ngIf=\"!dataTable.hasData\" layout=\"row\" layout-align=\"center center\">\n                    <h3>No results to display.</h3>\n                </div> \n                <td-paging-bar #pagingBar [pageSizes]=\"[5, 10, 15, 20]\" [total]=\"filteredTotal\" (change)=\"page($event)\">\n                    <span td-paging-bar-label hide-xs>Row per page:</span>\n                    {{pagingBar.range}} <span hide-xs>of {{pagingBar.total}}</span>\n                </td-paging-bar>\n            </md-card>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ 1025:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav logo=\"assets:covalent\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n    <span>Quickstart</span>\n    <span flex></span>\n    <a md-icon-button mdTooltip=\"Docs\" href=\"https://teradata.github.io/covalent/\" target=\"_blank\"><md-icon>chrome_reader_mode</md-icon></a>\n    <a md-icon-button mdTooltip=\"Github\" href=\"https://github.com/teradata/covalent\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <div layout-gt-sm=\"row\" tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['push-sm']\">\n    <div flex-gt-sm=\"60\">\n      <md-card>\n        <md-card-title>Products Sales</md-card-title>\n        <md-card-subtitle>usage stats for our products</md-card-subtitle>\n        <md-divider></md-divider>\n        <div class=\"chart-height push-top push-right-sm\">\n          <ngx-charts-area-chart-stacked\n            [scheme]=\"colorScheme\"\n            [results]=\"multi\"\n            [gradient]=\"gradient\"\n            [xAxis]=\"showXAxis\"\n            [yAxis]=\"showYAxis\"\n            [legend]=\"showLegend\"\n            [showXAxisLabel]=\"showXAxisLabel\"\n            [showYAxisLabel]=\"showYAxisLabel\"\n            [xAxisLabel]=\"xAxisLabel\"\n            [yAxisLabel]=\"yAxisLabel\"\n            [yAxisTickFormatting]=\"axisDigits\">\n          </ngx-charts-area-chart-stacked>\n        </div>\n      </md-card>\n      <div layout=\"row\">\n        <div flex=\"33\" layout=\"column\">\n          <md-card class=\"md-card-colored\" flex>\n            <md-card-content class=\"text-center\">\n              <div class=\"md-headline\">{{ '2341'| digits }} <md-icon class=\"tc-green-500 text-sm\">arrow_upward</md-icon></div>\n              <md-icon class=\"icon tc-indigo-700\">perm_contact_calendar</md-icon>\n              <div layout=\"column\">\n                <div class=\"md-subhead\">Active Users</div>\n                <div class=\"md-caption tc-grey-600\">updated {{ '2016-06-17 12:59:59' | timeAgo }}</div>\n              </div>\n            </md-card-content>\n          </md-card>\n        </div>\n        <div flex=\"33\" layout=\"column\">\n          <md-card class=\"md-card-colored\" flex>\n            <md-card-content class=\"text-center\">\n              <div class=\"md-headline\">{{ '72452903343' | bytes }} <md-icon class=\"tc-red-500 text-sm\">arrow_downward</md-icon></div>\n              <md-icon class=\"icon tc-light-blue-700\">sd_storage</md-icon>\n              <div layout=\"column\">\n                <div class=\"md-subhead\">User Disk Usage</div>\n                <div class=\"md-caption tc-grey-600\">updated {{ '2016-07-18 11:02:59' | timeAgo }}</div>\n              </div>\n            </md-card-content>\n          </md-card>\n        </div>\n        <div flex=\"33\" layout=\"column\">\n          <md-card class=\"md-card-colored\" flex>\n            <md-card-content class=\"text-center\">\n              <div class=\"md-headline\">{{ '324523' | digits}} <md-icon class=\"tc-green-500 text-sm\">arrow_upward</md-icon></div>\n              <md-icon class=\"icon tc-cyan-700\">receipt</md-icon>\n              <div layout=\"column\">\n                <div class=\"md-subhead\">Pages Viewed</div>\n                <div class=\"md-caption tc-grey-600\">updated {{ '2016-07-13 09:59:59' | timeAgo }}</div>\n              </div>\n            </md-card-content>\n          </md-card>\n        </div>\n      </div>\n      <md-card>\n        <md-card-title>Customer Activity</md-card-title>\n        <md-card-subtitle>Recent activity from select members</md-card-subtitle>\n        <md-divider></md-divider>\n        <md-nav-list class=\"will-load item-list\">\n          <ng-template tdLoading=\"items.load\">\n            <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"items\">\n              <a md-list-item layout-align=\"row\" [routerLink]=\"['../item', item.item_id]\">\n                <md-icon md-list-avatar> {{item.icon}} </md-icon>\n                <h3 md-line> {{item.name}} </h3>\n                <p md-line> {{item.description | truncate:50 }} </p>\n                <span flex></span>\n                <span class=\"md-caption text-right\" flex=\"20\"> {{item.created | timeAgo }} </span>\n              </a>\n              <md-divider *ngIf=\"!last\" md-inset></md-divider>\n            </ng-template>\n          </ng-template>\n        </md-nav-list>\n      </md-card>\n    </div>\n    <div flex-gt-sm=\"40\">\n      <md-card>\n        <md-card-title>Alerts</md-card-title>\n        <md-card-subtitle>Items requiring attention</md-card-subtitle>\n        <md-divider></md-divider>\n        <md-list class=\"will-load alert-list\">\n          <ng-template tdLoading=\"alerts.load\">\n            <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"alerts\">\n              <md-list-item layout-align=\"row\">\n                <md-icon md-list-avatar class=\"bgc-amber-800\">{{item.icon}}</md-icon>\n                <h3 md-line> {{item.description}} </h3>\n                <p md-line> {{item.name}} </p>\n              </md-list-item>\n              <md-divider *ngIf=\"!last\" md-inset></md-divider>\n            </ng-template>\n          </ng-template>\n        </md-list>\n        <md-divider></md-divider>\n        <md-card-actions>\n          <a md-button color=\"accent\" class=\"text-upper\" [routerLink]=\"['/logs']\">\n            <span>View More</span>\n          </a>\n        </md-card-actions>\n      </md-card>\n      <md-card id=\"dashboard-favorites-card\" class=\"push-top\">\n        <md-card-title>Favorites</md-card-title>\n        <md-card-subtitle>Your favorite products</md-card-subtitle>\n        <md-divider></md-divider>\n        <md-nav-list class=\"will-load favorite-list\">\n          <ng-template tdLoading=\"favorites.load\">\n            <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"products\">\n              <md-list-item layout-align=\"row\" [routerLink]=\"['/product']\">\n                <md-icon md-list-avatar>{{item.icon}}</md-icon>\n                <h3 md-line> {{item.name}} </h3>\n                <p md-line> {{item.description}} </p>\n              </md-list-item>\n              <md-divider *ngIf=\"!last\" md-inset></md-divider>\n            </ng-template>\n          </ng-template>\n        </md-nav-list>\n        <md-divider></md-divider>\n        <md-card-actions>\n          <a md-button color=\"accent\" class=\"text-upper\" [routerLink]=\"['/product']\">\n            <span>View More</span>\n          </a>\n        </md-card-actions>\n      </md-card>\n    </div>\n  </div>\n  <td-layout-footer>\n    <div layout=\"row\" layout-align=\"start center\">\n      <span class=\"md-caption\">Copyright &copy; 2016 Teradata. All rights reserved</span>\n      <span flex></span>\n      <md-icon class=\"md-icon-ux\" svgIcon=\"assets:teradata-ux\"></md-icon>\n    </div>\n  </td-layout-footer>\n</td-layout-nav>\n"

/***/ }),

/***/ 1026:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav>\n  <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n    <span *ngIf=\"item\">Activity > {{item.name}}</span>\n    <span flex></span>\n  </div>\n  <td-layout-card-over  *ngIf=\"item\">\n       <md-card-title>{{item.name}}'s Activity Stream</md-card-title>\n    <md-card-subtitle>Last activity: {{item.description}}</md-card-subtitle>\n    <md-divider></md-divider>\n    <md-list>\n      <ng-template ngFor [ngForOf]=\"[0,1,2,3,4]\">\n        <md-list-item layout-align=\"row\">\n            <md-icon md-list-avatar> receipt </md-icon>\n            <h3 md-line> {{item.name}} </h3>\n            <p md-line> activity description </p>            \n          </md-list-item>\n      </ng-template>\n    </md-list>\n    <md-divider></md-divider>\n    <md-card-actions>\n      <button md-button color=\"accent\" class=\"text-upper\" (click)=\"goBack()\">\n        Back\n      </button>\n    </md-card-actions>\n  </td-layout-card-over>   \n</td-layout-nav>"

/***/ }),

/***/ 1027:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav class=\"white\" logo=\"assets:covalent\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n    <span>Product Name</span>\n    <span flex></span>\n    <a md-icon-button mdTooltip=\"Docs\" href=\"https://teradata.github.io/covalent/\" target=\"_blank\"><md-icon>chrome_reader_mode</md-icon></a>\n    <a md-icon-button mdTooltip=\"Github\" href=\"https://github.com/teradata/covalent\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <td-layout-manage-list #manageList\n                        [opened]=\"media.registerQuery('gt-sm') | async\"\n                        [mode]=\"(media.registerQuery('gt-sm') | async) ? 'side' : 'push'\"\n                        [sidenavWidth]=\"(media.registerQuery('gt-xs') | async) ? '257px' : '100%'\">\n    <md-nav-list td-sidenav-content (click)=\"!media.query('gt-sm') && manageList.close()\">\n      <a md-list-item>\n        <md-icon>dashboard</md-icon>\n        Dashboard\n      </a>\n      <a md-list-item>\n        <md-icon>people</md-icon>\n        Customers\n      </a>\n      <a md-list-item>\n        <md-icon>receipt</md-icon>\n        Log\n      </a>\n      <a md-list-item>\n        <md-icon>settings</md-icon>\n        Settings\n      </a>\n    </md-nav-list>\n    <div td-toolbar-content layout=\"row\" layout-align=\"start center\" flex>\n      <a md-icon-button [routerLink]=\"['/product']\"><md-icon>arrow_back</md-icon></a>\n      <span>New Item</span>\n      <span flex></span>\n    </div>\n    <div class=\"md-content\" class=\"inset\">\n      <form>\n        <md-card>\n          <td-steps>\n            <td-step #step1 label=\"Step 1\" sublabel=\"simple metadata\" [disabled]=\"disabled\" (activated)=\"activeStep1Event()\" (deactivated)=\"deactiveStep1Event()\">\n              <div layout-gt-xs=\"row\">\n                <div class=\"pad\">\n                  <md-input-container>\n                    <input mdInput placeholder=\"Entry name (10 max)\" maxlength=\"10\" #inputHint>\n                    <md-hint align=\"end\">{{inputHint.value.length}} / 10</md-hint>\n                  </md-input-container>\n                </div>\n                <div class=\"pad\">\n                  <md-input-container>\n                    <input mdInput type=\"number\" placeholder=\"Budget\" align=\"end\">\n                    <span md-prefix>$&nbsp;</span>\n                    <span md-suffix>.00</span>\n                  </md-input-container>\n                </div>\n              </div>\n            </td-step>\n            <td-step #step2 label=\"Step 2\" sublabel=\"this step must be validated\" [state]=\"stateStep2\" [disabled]=\"disabled\">\n              <md-radio-group>\n                <md-radio-button value=\"option_1\">A New Hope</md-radio-button>\n                <md-radio-button value=\"option_2\">Empire Strikes Back</md-radio-button>\n              </md-radio-group>\n              <md-slide-toggle [checked]=\"true\">\n                Include Awesome\n              </md-slide-toggle>\n              <ng-template td-step-actions>\n                <button md-raised-button color=\"primary\" (click)=\"toggleRequiredStep2()\">Validate &amp; Proceed</button>\n                <button md-button (click)=\"step2.active = false\">Cancel</button>\n              </ng-template>\n            </td-step>\n            <td-step #step3 label=\"Step 3\" sublabel=\"this step will toggle complete\" [state]=\"stateStep3\" [disabled]=\"disabled\">\n              <md-checkbox [checked]=\"true\">\n                I agree to spread the word about Covalent\n              </md-checkbox>\n              <ng-template td-step-actions>\n                <button md-raised-button color=\"primary\" (click)=\"toggleCompleteStep3()\">Complete Step</button>\n                <button md-button (click)=\"step3.active = false\">Cancel</button>\n              </ng-template>\n              <ng-template td-step-summary>\n                You can add a summary that shows after a step is saved.\n              </ng-template>\n            </td-step>\n          </td-steps>\n          <md-divider></md-divider>\n          <md-card-actions>\n            <button md-raised-button color=\"accent\" [routerLink]=\"['/product']\">Save &amp; Create</button>\n            <button md-button class=\"tc-grey-600\"  [routerLink]=\"['/product']\">Cancel</button>\n          </md-card-actions>\n        </md-card>\n      </form>\n    </div>\n  </td-layout-manage-list>\n</td-layout-nav>\n"

/***/ }),

/***/ 1028:
/***/ (function(module, exports) {

module.exports = "<link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n<link href=\"https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css\" rel=\"stylesheet\">\n<link rel=\"stylesheet\" href=\"assets/font-awesome-4.7.0/css/font-awesome.min.css\">\n\n<style>\n\n\n.title {\n  position: absolute;\n  top: 74%;\n  left: 50%;\n  margin-right: -50%;\n  transform: translate(-50%, -50%);\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  text-align: center;\n  width: 960px;\n}\n\ncanvas {\n  position: absolute;\n  top: 57%;\n    left: 50%;\n    margin-right: -50%;\n    transform: translate(-50%, -50%);\n    z-index: -1;\n}\n\n.realtitle {\n  position: absolute;\n  top: -5px;\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  text-align: center;\n  width: 960px;\n  font-size: 17px;\n  color: #4099FF;\n  font-weight:bold;\n}\n\n.current-trend {\n\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  font-size: 26px;\n  left:80px;\n  color: #fff;\n}\n\n.trend {\n  color: #1DA1F2;\n  bottom:20px;\n}\n\n.inner-title {\n  font-size: 18px;\n  color: #fff;\n}\n\n.twitter-logo-a {\n  top:  40px;\n  left: 0px;\n}\n\n.twitter-logo-b {\n  position: absolute;\n  top: -17px;\n  left: 635px;\n}\n\nh1 {\n}\n\n.tweet {\n  position: absolute;\n  left: 240px;\n  width: 480px;\n  text-align: center;\n}\n\n.github-mark {\n  position: absolute;\n  left: 45px;\n  top: 25px;\n}\n\n.github {\n  position: absolute;\n  left: 25px;\n  top: 100px;\n}\n\na {\n  text-decoration: none;\n  font-family: \"Helvetica Neue\", Roboto, \"Segoe UI\", Calibri, sans-serif;\n  color: #1DA1F2\n}\n\nblockquote.twitter-tweet {\n  display: inline-block;\n  font-family: \"Helvetica Neue\", Roboto, \"Segoe UI\", Calibri, sans-serif;\n  font-size: 14px;\n  font-weight: bold;\n  line-height: 16px;\n  border-color: #eee #ddd #bbb;\n  border-radius: 5px;\n  border-style: solid;\n  border-width: 1px;\n  box-shadow: 0 1px 3px rgba(29, 161, 244, 0.84);\n  background-color: rgba(29, 161, 244, 0.84);\n  margin: 10px 5px;\n  padding: 0 16px 16px 16px;\n  max-width: 468px;\n}\n\nblockquote.twitter-tweet p {\n  font-size: 16px;\n  font-weight: normal;\n  line-height: 20px;\n}\n\nblockquote.twitter-tweet a {\n  color: inherit;\n  font-weight: normal;\n  font-family: \"Helvetica Neue\", Roboto, \"Segoe UI\", Calibri, sans-serif;\n  text-decoration: none;\n  outline: 0 none;\n}\n\nblockquote.twitter-tweet a:hover,\nblockquote.twitter-tweet a:focus {\n  text-decoration: underline;\n  color: #fff\n}\n\n.twitter-user {\n  color: #fff\n}\n\nmat-checkbox-label {\n  color: #1DA1F2\n}\n\n#pauseButton {\n  position: absolute;\n  left: 900px;\n}\n\n#playButton {\n  position: absolute;\n  left: 1000px;\n}\n\n#loadingCircle {\n  top: 50%;\n  left: 50%;\n  margin-right: -50%;\n  transform: translate(-50%, -50%);\n}\n\n.btn-group {\n  position:fixed;\n  float:right;\n    top:66px;\n    right:15px;\n}\n\n.current-trend-container {\n  position:fixed;\n  float:left;\n    top:80px;\n    left:165px;\n    padding: 0 1%;\n    max-width:100%;\n}\n\n.trendFlex {\n  flex: 1;\n  flex-grow: 2.5;\n}\n\n.globebody {\n  background-color: #fff;\n}\n\n.about-icon {\n  position:fixed;\n  float:right;\n  right:10px;\n}\n\n.btn-primary {\n  background: #a3e9db;\n  color: #ffffff;\n}\n\n.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active {\n  background: #338373;\n}\n\n</style>\n<td-layout #layout>\n\n<td-navigation-drawer sidenavTitle=\"Currently popular on Twitter:\" name=\"\"> <!-- email=\"Show tweets from:\" -->\n  <md-nav-list>\n    <a md-list-item><md-checkbox onclick=\"getTrendName(this.textContent)\" labelPosition=\"before\" color=\"warn\" checked=\"true\">Trump</md-checkbox></a>\n    <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"trends\">\n      <a md-list-item><md-checkbox onclick=\"getTrendName(this.textContent)\" labelPosition=\"before\" color=\"warn\">{{item}}</md-checkbox></a>\n    </ng-template>\n  </md-nav-list>\n</td-navigation-drawer>\n\n<td-layout-nav logo=\"\">\n\n  <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n\n    <!--<span style=\"color:#fff;font-weight:bold;\">Live tweets from around the world:&nbsp;</span>-->\n    <span flex>Live tweets from around the world! - <span class=\"current-trend\" style=\"font-weight:bold\"></span></span>\n\n  <span>\n\n    <!--<a md-icon-button mdTooltip=\"Docs\" href=\"https://teradata.github.io/covalent/\" target=\"_blank\"><md-icon>chrome_reader_mode</md-icon></a>-->\n    <a md-icon-button mdTooltip=\"About\" (click)=\"goToAbout()\" target=\"_blank\"><md-icon class=\"md-30\" color=\"accent\">info</md-icon></a>\n  </span>\n\n  </div>\n\n\n\n<div class=\"globebody\">\n  <div style=\"background-color:#fff;color:#fff;position:absolute;top: 30%; left: 50%;margin-right: -50%;transform: translate(-50%, -50%); height:170%; width:100%; z-index:-2;\">\n    placeholder\n  </div>\n  <!--<div class=\"current-trend\">-->\n\n      <!--Enter Search Term: <input type=\"text\" value=\"\" id=\"inputSearch\"><button type=\"submit\" onclick=\"newTerm();\">Update</button>-->\n  <!--</div>-->\n  <!-- <div class=\"pausePlay\">\n    <md-slide-toggle id=\"pausePlayButton\" checked=true color=\"primary\" onclick=\"pausePlayStream();\">\n      Pause | Play\n    </md-slide-toggle>\n  </div> -->\n\n  <div class=\"current-trend-container\">\n    <span class=\"current-trend\" style=\"position: absolute;font-family: Helvetica Neue, Helvetica, Arial, sans-serif; text-align:center; color:#338373;\">\n    </span>\n  </div>\n\n  <!--<button id=\"pauseButton\" type=\"button\" onclick=\"stopConnection();\"><i class=\"fa fa-pause\" style=\"color:#fff\"></i></button>\n  <button id=\"playButton\" type=\"button\" onclick=\"openConnectionAgain();\"><i class=\"fa fa-play\" style=\"color:#fff\"></i></button>-->\n  <div class=\"title\">\n    <p>\n      <md-spinner style=\"position:absolute;top: 30%;\n      left: 50%;\n      margin-right: -50%;\n      transform: translate(-50%, -50%);\" color=\"primary\"></md-spinner>\n    </p>\n  </div>\n\n  <div class=\"btn-group\" data-toggle=\"buttons\">\n  <label class=\"btn btn-sm btn-primary\" onclick=\"stopConnection();\">\n    <input type=\"radio\" name=\"options\" id=\"pause\" autocomplete=\"off\" > <i class=\"fa fa-pause\" style=\"color:#fff\"></i>\n  </label>\n  <label class=\"btn btn-sm btn-primary active\" onclick=\"openConnectionAgain();\">\n    <input type=\"radio\" name=\"options\" id=\"pause\" autocomplete=\"off\" checked> <i class=\"fa fa-play\" style=\"color:#fff\"></i>\n  </label>\n</div>\n  <!--<canvas id='rotatingGlobe' width='400' height='400'\n  style='width: 400px; height: 400px; cursor: move;'></canvas>-->\n</div>\n\n  <td-layout-footer>\n    <div layout=\"row\" layout-align=\"start center\">\n      <span class=\"footer-desc\" style=\"font-weight:bold;font-size:16px;\">\n        <div style=\"display:inline-block;\">\n          <a class=\"twitter-share-button\"\n            style=\"top:3px;color:#1DA1F3;\"\n            href=\"https://twitter.com/share\"\n            data-text=\"See what people around the world are tweeting about Trump and other trending topics!\"\n            data-url=\"http://globetweeting.com\"\n            data-hashtags=\"globetweeting,LiveTweets\"\n            data-related=\"twitterapi,twitter\">\n          Tweet about this!\n          </a>\n        </div>\n\n        <div class=\"fb-share-button\" style=\"display:inline-block;bottom:5px;\" data-href=\"http://globetweeting.com\" data-layout=\"button_count\" data-size=\"small\" data-mobile-iframe=\"true\"><a class=\"fb-xfbml-parse-ignore\" style=\"color:#3b5998;\" target=\"_blank\" href=\"https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fglobetweeting.com%2F&amp;src=sdkpreparse\">Share this on Facebook!</a></div>\n      </span>\n      <span flex></span>\n      <span class=\"copyright\">Copyright &copy; 2017 Globetweeting. All rights reserved.</span>\n    </div>\n  </td-layout-footer>\n</td-layout-nav>\n</td-layout>\n"

/***/ }),

/***/ 1029:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav-list #navList\n                    logo=\"assets:covalent\"\n                    toolbarTitle=\"Logs\"\n                    [opened]=\"media.registerQuery('gt-sm') | async\"\n                    [mode]=\"(media.registerQuery('gt-sm') | async) ? 'side' : 'push'\"\n                    [sidenavWidth]=\"(media.registerQuery('gt-xs') | async) ? '350px' : '100%'\">\n    <md-nav-list td-sidenav-content (click)=\"!media.query('gt-sm') && navList.close()\">\n        <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"products\">\n          <a md-list-item [routerLink]=\"['/logs']\">\n            <md-icon md-list-avatar>{{item.icon}}</md-icon>\n            <h3 md-line> {{item.name}} </h3>\n            <p md-line> product logs </p> \n          </a>\n          <md-divider *ngIf=\"!last\" md-inset></md-divider>\n        </ng-template>\n    </md-nav-list>\n    <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n      <span>All Product Logs</span>\n      <span flex></span>\n    </div>\n    <md-card tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['push']\">\n      <md-card-title>Logs</md-card-title>\n      <md-divider></md-divider>\n      <ng-template tdLoading=\"items.load\">\n        <md-list class=\"will-load\" >\n          <div class=\"md-padding\" *ngIf=\"!items || items.length === 0\" layout=\"row\" layout-align=\"center center\">\n            <h3>No logs to display.</h3>\n          </div>\n          <ng-template let-item let-last=\"last\" ngFor [ngForOf]=\"items\">\n            <md-list-item [title]=\"item.description\">\n              <md-icon md-list-icon> {{item.icon}} </md-icon>\n              <h3 md-line> {{item.description}} <span class=\"md-caption tc-grey-600\">({{item.created}})</span> </h3>\n              <p md-line > {{item.name}} </p>\n            </md-list-item>\n            <md-divider *ngIf=\"!last\"></md-divider>\n          </ng-template>\n        </md-list>\n      </ng-template>\n    </md-card>\n  </td-layout-nav-list>"

/***/ }),

/***/ 1030:
/***/ (function(module, exports) {

module.exports = "<td-layout>\n\n  <router-outlet></router-outlet>\n</td-layout>\n"

/***/ }),

/***/ 1031:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav logo=\"assets:covalent\" class=\"dark-grey-blue\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"start center\" flex>\n    <span>App Templates</span>\n    <span flex ></span>\n    <button md-icon-button [mdMenuTriggerFor]=\"notificationsMenu\">\n      <td-notification-count color=\"accent\" [notifications]=\"4\">\n        <md-icon>apps</md-icon>\n      </td-notification-count>\n    </button>\n    <md-menu #notificationsMenu=\"mdMenu\">\n      <td-menu>\n        <div td-menu-header class=\"md-subhead\">Templates</div>\n        <md-nav-list dense>\n          <a md-list-item [routerLink]=\"['/templates']\">\n            <md-icon md-list-avatar>system_update_alt</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Landing Page</span></h4>\n            <p md-line>a landing page template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/dashboard']\">\n            <md-icon md-list-avatar>dashboard</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Dashboard</span></h4>\n            <p md-line>an ngx-charts dashboard template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/email']\">\n            <md-icon md-list-avatar>email</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Email App</span></h4>\n            <p md-line>an email app template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/editor']\">\n            <md-icon md-list-avatar>view_array</md-icon>\n            <h4 md-line><span class=\"text-wrap\">IDE Editor</span></h4>\n            <p md-line>an IDE text editor app template</p>\n          </a>\n        </md-nav-list>\n        <a md-button color=\"accent\" td-menu-footer href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\">\n          View Code\n        </a>\n      </td-menu>\n    </md-menu>\n    <a md-icon-button mdTooltip=\"View this code\" href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <div class=\"bgc-black inset charts-dark\" flex>\n    <div layout-gt-xs=\"row\" layout-wrap>\n      <div flex-gt-xs=\"40\">\n        <md-card>\n          <md-card-title>Total Sales</md-card-title>\n          <md-card-subtitle>combined sales for all products</md-card-subtitle>\n          <md-divider></md-divider>\n          <div style=\"height:290px;overflow:hidden;\" class=\"pad-top-sm\">\n            <ngx-charts-area-chart-stacked\n              [scheme]=\"colorScheme\"\n              [results]=\"multi\"\n              [gradient]=\"gradient\"\n              [xAxis]=\"showXAxis\"\n              [yAxis]=\"showYAxis\"\n              [legend]=\"showLegend\"\n              [showXAxisLabel]=\"showXAxisLabel\"\n              [showYAxisLabel]=\"showYAxisLabel\"\n              [xAxisLabel]=\"xAxisLabel\"\n              [yAxisLabel]=\"yAxisLabel\">\n            </ngx-charts-area-chart-stacked>\n          </div>\n        </md-card>\n      </div>\n      <div flex-gt-xs=\"60\">\n        <div layout-gt-xs=\"row\">\n          <div flex-gt-xs=\"100\">\n            <ngx-charts-number-card\n              [scheme]=\"colorSchemeDark\"\n              [results]=\"single\">\n            </ngx-charts-number-card>\n          </div>\n        </div>\n        <div layout-gt-xs=\"row\" class=\"pull-top-xs\">\n          <div flex-gt-xs=\"100\">\n            <md-card>\n              <div style=\"height:224px;overflow:hidden;\" class=\"pad-top push-right\">\n                <ngx-charts-bar-horizontal-stacked\n                  [scheme]=\"colorScheme\"\n                  [results]=\"multi\"\n                  [gradient]=\"gradient\"\n                  [xAxis]=\"showXAxis\"\n                  [yAxis]=\"showYAxis\"\n                  [legend]=\"showLegend\"\n                  [showXAxisLabel]=\"showXAxisLabel\"\n                  [showYAxisLabel]=\"showYAxisLabel\"\n                  [xAxisLabel]=\"xAxisLabel\">\n                </ngx-charts-bar-horizontal-stacked>\n              </div>\n            </md-card>\n          </div>\n        </div>\n      </div>\n      <div flex-gt-xs=\"33\">\n        <md-card>\n          <md-card-title>Product Sales</md-card-title>\n          <md-divider></md-divider>\n          <div style=\"height:250px;\" class=\"pad-top\">\n            <ngx-charts-bar-vertical-2d\n              [scheme]=\"colorScheme\"\n              [results]=\"multi\"\n              [gradient]=\"gradient\"\n              [xAxis]=\"showXAxis\"\n              [yAxis]=\"showYAxis\"\n              [legend]=\"showLegend\"\n              [showXAxisLabel]=\"showXAxisLabel\"\n              [showYAxisLabel]=\"showYAxisLabel\"\n              [xAxisLabel]=\"xAxisLabel\"\n              [yAxisLabel]=\"yAxisLabel\">\n            </ngx-charts-bar-vertical-2d>\n          </div>\n        </md-card>\n      </div>\n      <div flex-gt-xs=\"33\">\n        <md-card>\n          <md-card-title>Product Usage</md-card-title>\n          <md-divider></md-divider>\n          <div style=\"height:250px;\" class=\"pad-top\">\n            <ngx-charts-gauge\n              [scheme]=\"colorScheme\"\n              [results]=\"single\"\n              [min]=\"0\"\n              [max]=\"100\"\n              [units]=\"'usage'\"\n              [bigSegments]=\"10\"\n              [smallSegments]=\"5\">\n            </ngx-charts-gauge>\n          </div>\n        </md-card>\n      </div>\n      <div flex-gt-xs=\"33\">\n        <md-card>\n          <md-card-title>Sales Forecast</md-card-title>\n          <md-divider></md-divider>\n          <div style=\"height:250px;\" class=\"pad-top\">\n            <ngx-charts-line-chart\n              [scheme]=\"colorScheme\"\n              [results]=\"multi\"\n              [gradient]=\"gradient\"\n              [xAxis]=\"showXAxis\"\n              [yAxis]=\"showYAxis\"\n              [legend]=\"showLegend\"\n              [showXAxisLabel]=\"showXAxisLabel\"\n              [showYAxisLabel]=\"showYAxisLabel\"\n              [xAxisLabel]=\"xAxisLabel\"\n              [yAxisLabel]=\"yAxisLabel\"\n              [autoScale]=\"autoScale\">\n            </ngx-charts-line-chart>\n          </div>\n        </md-card>\n      </div>\n    </div>\n  </div>\n  <td-layout-footer>\n    <div layout=\"row\" layout-align=\"start center\">\n      <span class=\"md-caption\">Days Shown:</span>\n      <md-slider flex thumbLabel tickInterval=\"1\" step=\"1\" min=\"1\" max=\"30\" value=\"14\"></md-slider>\n    </div>\n  </td-layout-footer>\n  <a md-fab color=\"accent\" class=\"md-fab-position-bottom-right\" style=\"bottom:20px;\" href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\"> \n    <md-icon>code</md-icon>\n  </a>\n</td-layout-nav>\n"

/***/ }),

/***/ 1032:
/***/ (function(module, exports) {

module.exports = "<div layout=\"column\" layout-fill class=\"dark-grey-blue\">\n    <div flex layout=\"row\" class=\"content md-content\">\n        <div>test</div>\n        <md-sidenav-container fullscreen class=\"td-layout-manage-list md-content\" flex layout=\"row\">\n            <md-sidenav opened=\"true\" align=\"start\" mode=\"side\" class=\"md-sidenav-left\">\n                <div layout=\"column\">\n                    <div flex layout=\"column\">\n                        <div layout=\"row\" layout-align=\"start center\" class=\"bgc-grey-900\">\n                            <span class=\"md-subhead push-left-sm\">Data Sources</span>\n                            <span flex></span>\n                            <button md-icon-button><md-icon>sort</md-icon></button>\n                            <button md-icon-button><md-icon>settings</md-icon></button>\n                            <button md-icon-button><md-icon>more_vert</md-icon></button>\n                        </div>\n                        <div layout=\"column\" layout-margin class=\"md-content\">\n                            <md-nav-list dense>\n                                <h3 md-subheader>Database Connections</h3>\n                                <a md-list-item class=\"active\">\n                                    <md-icon md-list-icon>folder_open</md-icon>\n                                    Teradata Databases\n                                </a>\n                                <md-nav-list dense class=\"push-left\">\n                                    <a md-list-item>\n                                        <md-icon md-list-icon>dns</md-icon>\n                                        DBC\n                                    </a>\n                                    <a md-list-item class=\"active\">\n                                        <md-icon md-list-icon>dns</md-icon>\n                                        PROD_DATA\n                                    </a>\n                                    <a md-list-item>\n                                        <md-icon md-list-icon>dns</md-icon>\n                                        store_locals\n                                    </a>\n                                    <a md-list-item>\n                                        <md-icon md-list-icon>dns</md-icon>\n                                        sales_trending_reports\n                                    </a>\n                                </md-nav-list>\n                                <a md-list-item>\n                                    <md-icon md-list-icon>folder</md-icon>\n                                    Presto Databases\n                                </a>\n                                <a md-list-item>\n                                    <md-icon md-list-icon>folder</md-icon>\n                                    Aster Databases\n                                </a>\n                                <a md-list-item>\n                                    <md-icon md-list-icon>folder</md-icon>\n                                    Hadoop Databases\n                                </a>\n                            </md-nav-list>\n                        </div>\n                    </div>\n                    <div flex layout=\"column\">\n                        <div layout=\"row\" layout-align=\"start center\" class=\"bgc-grey-900\">\n                            <span class=\"md-subhead push-left-sm\">File Explorer</span>\n                            <span flex></span>\n                            <button md-icon-button><md-icon>sort</md-icon></button>\n                            <button md-icon-button><md-icon>settings</md-icon></button>\n                            <button md-icon-button><md-icon>more_vert</md-icon></button>\n                        </div>\n                        <div layout=\"column\" layout-margin class=\"md-content\">\n                            <md-nav-list dense>\n                                <a md-list-item>\n                                    <md-icon md-list-icon>folder</md-icon>\n                                    Backup Queries\n                                </a>\n                                <a md-list-item>\n                                    <md-icon md-list-icon>folder</md-icon>\n                                    Downloads\n                                </a>\n                                <a md-list-item class=\"active\">\n                                    <md-icon md-list-icon>folder_open</md-icon>\n                                    My Queries\n                                </a>\n                                <md-nav-list dense class=\"push-left\">\n                                    <a md-list-item>\n                                        <md-icon md-list-icon>description</md-icon>\n                                        DBC\n                                    </a>\n                                    <a md-list-item class=\"active\">\n                                        <md-icon md-list-icon>description</md-icon>\n                                        PROD_DATA\n                                    </a>\n                                    <a md-list-item>\n                                        <md-icon md-list-icon>description</md-icon>\n                                        store_locals\n                                    </a>\n                                    <a md-list-item>\n                                        <md-icon md-list-icon>description</md-icon>\n                                        sales_trending_reports\n                                    </a>\n                                </md-nav-list>\n                            </md-nav-list>\n                        </div>\n                    </div>\n                    <div class=\"td-layout-footer bgc-black pad-xs\" layout=\"row\">\n                        <button md-icon-button><md-icon>add</md-icon></button>\n                        <span flex></span>\n                        <button md-icon-button><md-icon>refresh</md-icon></button>\n                    </div>\n                </div>\n            </md-sidenav>\n            <md-sidenav opened align=\"end\" mode=\"side\" class=\"md-sidenav-right\">\n                <div layout=\"column\">\n                    <div flex layout=\"column\">\n                        <div layout=\"row\" layout-align=\"start center\" class=\"bgc-grey-900\">\n                            <span class=\"md-subhead push-left-sm\">Connection Filters</span>\n                            <span flex></span>\n                            <button md-icon-button><md-icon>more_vert</md-icon></button>\n                        </div>\n                        <div layout=\"column\" layout-margin class=\"md-content\">\n                            <md-tab-group md-stretch-tabs>\n                                <md-tab>\n                                    <ng-template md-tab-label>\n                                        Selection\n                                    </ng-template>\n                                    <div layout=\"row\" class=\"push-top-sm\">\n                                        <md-select placeholder=\"Select option\" flex>\n                                            <md-option value=\"1\">\n                                                Include selected options\n                                            </md-option>\n                                            <md-option value=\"2\">\n                                                Include non-selected options\n                                            </md-option>\n                                        </md-select>\n                                    </div>\n                                    <div class=\"pad\">\n                                        <md-slide-toggle color=\"accent\">appcenter-apps</md-slide-toggle>\n                                        <md-slide-toggle color=\"accent\" checked=\"true\">employee%</md-slide-toggle>\n                                        <md-slide-toggle color=\"accent\">jsonDefault</md-slide-toggle>\n                                        <md-slide-toggle color=\"accent\" checked=\"true\">jsonTable</md-slide-toggle>\n                                        <md-slide-toggle color=\"accent\" checked=\"true\">listener_data</md-slide-toggle>\n                                        <md-slide-toggle color=\"accent\">retailWebClicksData</md-slide-toggle>\n                                        <md-slide-toggle color=\"accent\">rowdatasave</md-slide-toggle>\n                                    </div>\n                                </md-tab>\n                                <md-tab>\n                                    <ng-template md-tab-label>\n                                        Expression\n                                    </ng-template>\n                                    <h1>Terrible sushi restaurants</h1>\n                                    <p>...</p>\n                                </md-tab>\n                            </md-tab-group>\n                        </div>\n                    </div>\n                    <div flex layout=\"column\">\n                        <div layout=\"row\" layout-align=\"start center\" class=\"bgc-grey-900\">\n                            <span class=\"md-subhead push-left-sm\">Visualization Options</span>\n                            <span flex></span>\n                            <button md-icon-button><md-icon>more_vert</md-icon></button>\n                        </div>\n                        <div layout=\"column\" layout-margin class=\"md-content\">\n                            <div layout=\"row\">\n                                <md-select placeholder=\"Select type\" flex>\n                                    <md-option value=\"1\">\n                                        Vertical Bar\n                                    </md-option>\n                                    <md-option value=\"2\">\n                                        Horizontal Bar\n                                    </md-option>\n                                </md-select>\n                            </div>\n                            <div class=\"pad\" layout=\"column\">\n                                <md-checkbox checked=\"true\" class=\"push-bottom-sm\">Show X Axis</md-checkbox>\n                                <md-checkbox checked=\"true\" class=\"push-bottom-sm\">Show Y Axis</md-checkbox>\n                                <md-checkbox checked=\"true\" class=\"push-bottom-sm\">Show Grid Lines</md-checkbox>\n                                <md-checkbox class=\"push-bottom-sm\">Use Gradients</md-checkbox>\n                                <md-checkbox class=\"push-bottom-sm\">Show Legend</md-checkbox>\n                                <md-checkbox checked=\"true\" class=\"push-bottom-sm\">Show X Axis Label</md-checkbox>\n                                <md-checkbox class=\"push-bottom-sm\">Show Y Axis Label</md-checkbox>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"td-layout-footer bgc-black pad-xs\" layout=\"row\">\n                        <button md-icon-button><md-icon>add</md-icon></button>\n                        <span flex></span>\n                        <button md-icon-button><md-icon>refresh</md-icon></button>\n                    </div>\n                </div>\n            </md-sidenav>\n            <div layout=\"column\" flex layout-fill>\n                <div flex layout=\"column\">\n                    <div flex layout=\"column\">\n                        <div layout=\"row\" layout-align=\"start center\" class=\"bgc-grey-900\">\n                            <nav md-tab-nav-bar>\n                                <a md-tab-link>\n                                    <div layout=\"row\" layout-align=\"start center\">\n                                        <md-icon class=\"text-md\">description</md-icon>\n                                        <span class=\"text-md\">filename.sql</span>\n                                        <span flex></span>\n                                        <md-icon class=\"text-md\">close</md-icon>\n                                    </div>\n                                </a>\n                                <a md-tab-link>\n                                    <div layout=\"row\" layout-align=\"start center\">\n                                        <md-icon class=\"text-md\">description</md-icon>\n                                        <span class=\"text-md\">filename.sql</span>\n                                        <span flex></span>\n                                        <md-icon class=\"text-md\">close</md-icon>\n                                    </div>\n                                </a>\n                            </nav>\n                            <span flex></span>\n                            <button md-icon-button [mdMenuTriggerFor]=\"notificationsMenu\">\n                                <td-notification-count color=\"accent\" [notifications]=\"4\">\n                                    <md-icon>apps</md-icon>\n                                </td-notification-count>\n                                </button>\n                                <md-menu #notificationsMenu=\"mdMenu\" x-position=\"before\">\n                                    <td-menu>\n                                        <div td-menu-header class=\"md-subhead\">Templates</div>\n                                        <md-nav-list dense>\n                                        <a md-list-item [routerLink]=\"['/templates']\">\n                                            <md-icon md-list-avatar>system_update_alt</md-icon>\n                                            <h4 md-line><span class=\"text-wrap\">Landing Page</span></h4>\n                                            <p md-line>a landing page template</p>\n                                        </a>\n                                        <md-divider></md-divider>\n                                        <a md-list-item [routerLink]=\"['/templates/dashboard']\">\n                                            <md-icon md-list-avatar>dashboard</md-icon>\n                                            <h4 md-line><span class=\"text-wrap\">Dashboard</span></h4>\n                                            <p md-line>an ngx-charts dashboard template</p>\n                                        </a>\n                                        <md-divider></md-divider>\n                                        <a md-list-item [routerLink]=\"['/templates/email']\">\n                                            <md-icon md-list-avatar>email</md-icon>\n                                            <h4 md-line><span class=\"text-wrap\">Email App</span></h4>\n                                            <p md-line>an email app template</p>\n                                        </a>\n                                        <md-divider></md-divider>\n                                        <a md-list-item [routerLink]=\"['/templates/editor']\">\n                                            <md-icon md-list-avatar>view_array</md-icon>\n                                            <h4 md-line><span class=\"text-wrap\">IDE Editor</span></h4>\n                                            <p md-line>an IDE text editor app template</p>\n                                        </a>\n                                        </md-nav-list>\n                                        <a md-button color=\"accent\" td-menu-footer href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\">\n                                        View Code\n                                        </a>\n                                    </td-menu>\n                                </md-menu>\n                            <button md-icon-button><md-icon>more_vert</md-icon></button>\n                        </div>\n                        <td-highlight lang=\"sql\" flex class=\"md-content\">\n                            While EXISTS(SELECT * From @documents WHERE @lastidprocessed < P_Id)\n                            Begin\n                            Select Top 1 @id = P_Id, @document = Content From @documents WHERE @lastidprocessed < P_Id\n                            set @istart = 1; \n                                set @len = LEN(@document);\n                            \n                            -- For every word within a document \n                                While (@istart <= @len)\n                                Begin \n                            --pos := instr(@document, sep, @istart); \n                            set @pos = CHARINDEX ( @sep ,@document, @istart )\n                            if (@pos = 0)\n                            begin\n                                set @word = SUBSTRING(@document, @istart, @len); \n                                insert into @t values ( @wordcounter, @word )\n                                set @istart = @len + 1; \n                                set @wordcounter = @wordcounter + 1;\n                            end\n                            else\n                            begin \n                                set @word = SUBSTRING(@document, @istart, @pos - @istart); \n                                insert into @t values ( @wordcounter, @word )\n                                set @istart = @pos + 1; \n                                set @wordcounter = @wordcounter + 1;\n                            end\n                            End \n                            \n                            --Delete #tmpTable Where Indx = @id\n                            set @lastidprocessed = @id\n\n                            if (@word <> @previousword)\n                            begin\n                            If (@previousword <> '')\n                            begin\n                            insert into @t values ( @previousword, @wordoccurrences );\n                            end\n                            set @wordoccurrences = 1;\n                            end\n                            else\n                            begin \n                            set @wordoccurrences = @wordoccurrences + 1;\n                            end \n                                \n                            set @previousword = @word;\n                            set @lastidprocessed = @id\n                            End\n                        </td-highlight>\n                    </div>\n                    <div flex layout=\"column\">\n                        <div layout=\"row\" layout-align=\"start center\" class=\"bgc-grey-900\">\n                            <span class=\"md-subhead push-left-sm\">Result Set</span>\n                            <span flex></span>\n                            <button md-icon-button><md-icon>view_module</md-icon></button>\n                            <button md-icon-button><md-icon>sort</md-icon></button>\n                            <button md-icon-button><md-icon>settings</md-icon></button>\n                            <button md-icon-button><md-icon>more_vert</md-icon></button>\n                        </div>\n                        <div layout=\"column\" layout-margin class=\"md-content\">\n                            <div style=\"height:250px;\" class=\"pad-top\">\n                                <ngx-charts-line-chart\n                                [scheme]=\"colorScheme\"\n                                [results]=\"multi\"\n                                [gradient]=\"gradient\"\n                                [xAxis]=\"showXAxis\"\n                                [yAxis]=\"showYAxis\"\n                                [legend]=\"showLegend\"\n                                [showXAxisLabel]=\"showXAxisLabel\"\n                                [showYAxisLabel]=\"showYAxisLabel\"\n                                [xAxisLabel]=\"xAxisLabel\"\n                                [yAxisLabel]=\"yAxisLabel\"\n                                [autoScale]=\"autoScale\">\n                                </ngx-charts-line-chart>\n                            </div>\n                        </div>\n                    </div>\n                    <div flex layout=\"column\">\n                        <div layout=\"row\" layout-align=\"start center\" class=\"bgc-grey-900\">\n                            <span *ngIf=\"!searchBox.searchVisible\">\n                                <span class=\"md-subhead push-left-sm\">Data Preview</span>\n                            </span>\n                            <td-search-box #searchBox backIcon=\"arrow_back\" class=\"pull-top-sm pull-bottom-sm\" placeholder=\"Search here\" (searchDebounce)=\"search($event)\" flex>\n                            </td-search-box>\n                            <button md-icon-button><md-icon>view_module</md-icon></button>\n                            <button md-icon-button><md-icon>sort</md-icon></button>\n                            <button md-icon-button><md-icon>settings</md-icon></button>\n                            <button md-icon-button><md-icon>more_vert</md-icon></button>\n                        </div>\n                        <td-data-table\n                            #dataTable\n                            [data]=\"filteredData\"\n                            [columns]=\"columns\"\n                            [sortable]=\"true\"\n                            [sortBy]=\"sortBy\"\n                            [sortOrder]=\"sortOrder\"\n                            (sortChange)=\"sort($event)\"\n                            flex class=\"md-content\">\n                        </td-data-table>\n                        <div class=\"md-padding\" *ngIf=\"!dataTable.hasData\" layout=\"row\" layout-align=\"center center\">\n                            <h3>No results to display.</h3>\n                        </div>\n                        <td-paging-bar #pagingBar [pageSizes]=\"[5, 10, 15, 20]\" [total]=\"filteredTotal\" (change)=\"page($event)\">\n                            <span td-paging-bar-label hide-xs>Row per page:</span>\n                            {{pagingBar.range}} <span hide-xs>of {{pagingBar.total}}</span>\n                        </td-paging-bar>\n                    </div>\n                </div>\n                <div class=\"td-layout-footer pad-xs\">\n                    <div layout=\"row\" layout-align=\"start center\">\n                        <span class=\"push-left\">\n                            <md-icon class=\"text-md\">description</md-icon>\n                            <span>local/my queries/filename.sql</span>\n                        </span>\n                        <span flex></span>\n                        <span class=\"push-left\">\n                            <md-icon class=\"text-md\">warning</md-icon>\n                            <span>8</span>\n                        </span>\n                        <span class=\"push-left\">\n                            <md-icon class=\"text-md\">error</md-icon>\n                            <span>2</span>\n                        </span>\n                    </div>\n                </div>\n                <md-divider></md-divider>\n            </div>\n        </md-sidenav-container>\n    </div>\n    <div class=\"td-layout-footer pad-sm\">\n        <div layout=\"row\" layout-align=\"start center\">\n            <span class=\"tc-green-600\">\n                <md-icon class=\"text-md\">check</md-icon>\n                <span>No Issues</span>\n            </span>\n            <span flex></span>\n            <span class=\"push-left tc-blue-600\">\n                <md-icon class=\"text-md\">system_update_alt</md-icon>\n                <span>2 Updates</span>\n            </span>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ 1033:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav-list #navList\n                    logo=\"assets:covalent\"\n                    toolbarTitle=\"Email App\"\n                    class=\"light-blue-red\"\n                    [opened]=\"media.registerQuery('gt-sm') | async\"\n                    [mode]=\"(media.registerQuery('gt-sm') | async) ? 'side' : 'push'\"\n                    [sidenavWidth]=\"(media.registerQuery('gt-xs') | async) ? '350px' : '100%'\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"start center\" flex>\n    <span flex *ngIf=\"!searchBox.searchVisible\"></span>\n    <td-search-box #searchBox backIcon=\"arrow_back\" class=\"pull-top-sm pull-bottom-sm\" placeholder=\"Search here\" flex>\n    </td-search-box>\n    <button md-icon-button [mdMenuTriggerFor]=\"notificationsMenu\">\n      <td-notification-count color=\"accent\" [notifications]=\"4\">\n        <md-icon>apps</md-icon>\n      </td-notification-count>\n    </button>\n    <md-menu #notificationsMenu=\"mdMenu\">\n      <td-menu>\n        <div td-menu-header class=\"md-subhead\">Templates</div>\n        <md-nav-list dense>\n          <a md-list-item [routerLink]=\"['/templates']\">\n            <md-icon md-list-avatar>system_update_alt</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Landing Page</span></h4>\n            <p md-line>a landing page template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/dashboard']\">\n            <md-icon md-list-avatar>dashboard</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Dashboard</span></h4>\n            <p md-line>an ngx-charts dashboard template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/email']\">\n            <md-icon md-list-avatar>email</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Email App</span></h4>\n            <p md-line>an email app template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/editor']\">\n            <md-icon md-list-avatar>view_array</md-icon>\n            <h4 md-line><span class=\"text-wrap\">IDE Editor</span></h4>\n            <p md-line>an IDE text editor app template</p>\n          </a>\n        </md-nav-list>\n        <a md-button color=\"accent\" td-menu-footer href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\">\n          View Code\n        </a>\n      </td-menu>\n    </md-menu>\n    <a md-icon-button mdTooltip=\"View this code\" href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <a md-fab td-sidenav-content color=\"accent\" class=\"md-fab-position-bottom-right\" style=\"bottom:20px; position: fixed;\"> \n    <md-icon>add</md-icon>\n  </a>\n  <md-nav-list td-sidenav-content (click)=\"!media.query('gt-sm') && navList.close()\">\n      <ng-template let-item let-i=\"index\" let-last=\"last\" ngFor [ngForOf]=\"[0,1,2,3,4,5,6,7,8,9]\">\n        <a md-list-item>\n          <img md-list-avatar src=\"http://lorempixel.com/40/40/people/{{i}}\" />\n          <h3 md-line> Firstname Lastname </h3>\n          <p md-line> Email description goes here </p>\n          <md-icon class=\"tc-amber-800\">fiber_new</md-icon>\n        </a>\n        <md-divider *ngIf=\"!last\" md-inset></md-divider>\n      </ng-template>\n  </md-nav-list>\n  <md-card tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['push']\">\n    <md-card-header>\n      <img md-card-avatar src=\"http://lorempixel.com/40/40/people/9\" />\n      <md-card-title>Firstname Lastname</md-card-title>\n      <md-card-subtitle>Tuesday, January 3 2017 at 8:39 PM</md-card-subtitle>\n      <span flex></span>\n      <div class=\"pad-top pad-right\">\n        <button md-icon-button><md-icon>reply</md-icon></button>\n        <button md-icon-button><md-icon>forward</md-icon></button>\n        <button md-icon-button><md-icon>more_vert</md-icon></button>\n      </div>\n    </md-card-header>\n    <md-divider></md-divider>\n    <div class=\"md-padding\">\n      <p>\n        Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.\n      </p>\n      <p>\n        Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.\n      </p>\n      <p>\n        Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.\n      </p>\n      <p>\n        Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence on cross-platform integration.\n      </p>\n      <p>\n        Sincerly<br>-Kimmie\n      </p>\n    </div>\n    <md-divider></md-divider>\n    <div layout=\"row\" layout-align=\"space-around center\" class=\"pad\">\n      <button md-button class=\"tc-grey-600\">\n        <md-icon class=\"pull-bottom\">reply</md-icon>\n        <div class=\"md-caption\">Reply</div>\n      </button>\n      <button md-button class=\"tc-grey-600\">\n        <md-icon class=\"pull-bottom\">reply_all</md-icon>\n        <div class=\"md-caption\">Reply All</div>\n      </button>\n      <button md-button class=\"tc-grey-600\">\n        <md-icon class=\"pull-bottom\">forward</md-icon>\n        <div class=\"md-caption\">Forward</div>\n      </button>\n    </div>\n  </md-card>\n</td-layout-nav-list>"

/***/ }),

/***/ 1034:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav logo=\"assets:covalent\" class=\"white-orange\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"start center\" flex>\n    <span hide-xs>App Templates</span>\n    <span flex ></span>\n    <button md-icon-button [mdMenuTriggerFor]=\"notificationsMenu\">\n      <td-notification-count color=\"accent\" [notifications]=\"4\">\n        <md-icon>apps</md-icon>\n      </td-notification-count>\n    </button>\n    <md-menu #notificationsMenu=\"mdMenu\">\n      <td-menu>\n        <div td-menu-header class=\"md-subhead\">Templates</div>\n        <md-nav-list dense>\n          <a md-list-item [routerLink]=\"['/templates']\">\n            <md-icon md-list-avatar>system_update_alt</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Landing Page</span></h4>\n            <p md-line>a landing page template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/dashboard']\">\n            <md-icon md-list-avatar>dashboard</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Dashboard</span></h4>\n            <p md-line>an ngx-charts dashboard template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/email']\">\n            <md-icon md-list-avatar>email</md-icon>\n            <h4 md-line><span class=\"text-wrap\">Email App</span></h4>\n            <p md-line>an email app template</p>\n          </a>\n          <md-divider></md-divider>\n          <a md-list-item [routerLink]=\"['/templates/editor']\">\n            <md-icon md-list-avatar>view_array</md-icon>\n            <h4 md-line><span class=\"text-wrap\">IDE Editor</span></h4>\n            <p md-line>an IDE text editor app template</p>\n          </a>\n        </md-nav-list>\n        <a md-button color=\"accent\" td-menu-footer href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\">\n          View Code\n        </a>\n      </td-menu>\n    </md-menu>\n    <a md-icon-button mdTooltip=\"View this code\" href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <md-toolbar color=\"primary\" class=\"pad-none pull-top\">\n    <nav md-tab-nav-bar class=\"pull-bottom\">\n      <a md-tab-link\n        [routerLink]=\"['/templates']\"\n        class=\"active\">\n        Landing Page\n      </a>\n      <a md-tab-link\n        [routerLink]=\"['/templates/dashboard']\">\n        Dashboard\n      </a>\n      <a md-tab-link\n        [routerLink]=\"['/templates/email']\">\n        Email App\n      </a>\n      <a md-tab-link\n        [routerLink]=\"['/templates/editor']\">\n        IDE Editor\n      </a>\n    </nav>\n  </md-toolbar>\n  <section class=\"bgc-blue-50\">\n    <div layout-gt-xs=\"row\">\n      <div flex class=\"pad\" tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['pad-xl']\">\n        <div class=\"push-top-xl\" hide-xs></div>\n        <div tdMediaToggle=\"gt-sm\" [mediaClasses]=\"['push-lg']\">\n          <h1 class=\"md-display-1 tc-grey-800 push-top-xs push-bottom-sm\">Awesome Landing Page Template</h1>\n          <p class=\"md-title tc-grey-600\">Covalent isn't just for web application interfaces. You can easily build a reponsive landing pages just like this!</p>\n            <button md-raised-button color=\"accent\" class=\"push-bottom md-button-lg\">Get Started</button>\n        </div>\n        <div class=\"push-bottom-xl\" hide-xs></div>\n      </div>\n      <div flex=\"55\" class=\"bg-cover\" style=\"background-image:url('https://assets.entrepreneur.com/static/1425479423-vince-vaughn-appearing-free-cheesy-stock-images-you-can-download-getty-3.jpg');background-size:cover;\">\n      </div>\n    </div>\n  </section>\n\n  <section class=\"bgc-light-blue-600 tc-light-blue-50\"  tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['pad-top-xl', 'pad-bottom-xl']\">\n    <h2 class=\"md-headline text-upper text-center tc-grey-50 push-bottom\">Covalent Templates</h2>\n    <div layout=\"row\" layout-align-gt-xs=\"center center\">\n      <div flex-gt-xs=\"80\" layout=\"row\" layout-wrap>\n        <div class=\"text-center\" flex-xs=\"50\" flex-gt-xs=\"25\">\n          <md-icon class=\"tc-grey-50 md-display-2\">system_update_alt</md-icon>\n          <div class=\"pad-bottom-md\"> Landing Page Template</div>\n        </div>\n        <div class=\"text-center\" flex-xs=\"50\" flex-gt-xs=\"25\">\n          <md-icon class=\"tc-grey-50 md-display-2 \">dashboard</md-icon>\n          <div class=\"pad-bottom-md\">Dashboard Template</div>\n        </div>\n        <div class=\"text-center\" flex-xs=\"50\" flex-gt-xs=\"25\">\n          <md-icon class=\"tc-grey-50 md-display-2\">email</md-icon>\n          <div class=\"pad-bottom-md\">Email App Template</div>\n        </div>\n        <div class=\"text-center\" flex-xs=\"50\" flex-gt-xs=\"25\">\n          <md-icon class=\"tc-grey-50 md-display-2\">view_array</md-icon>\n          <div class=\"pad-bottom-md\">IDE Editor Template</div>\n        </div>\n      </div>\n    </div>\n  </section>\n<!--Product Card code starts here-->\n  <section class=\"bgc-blue-grey-50\" tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['pad-top-xl', 'pad-bottom-xl']\">\n    <div  layout-gt-xs=\"row\" layout-align-gt-xs=\"center center\">\n      <div layout-gt-sm=\"row\" flex-gt-xs=\"75\" layout-margin>\n        <div flex-gt-xs=\"25\">\n          <md-card>\n            <md-toolbar color=\"accent\" class=\"bgc-light-blue-500 push-bottom\">\n              <span class=\"text-upper\">Plan Name</span>\n            </md-toolbar>\n            <md-card-subtitle>Plan description goes here.</md-card-subtitle>\n            <md-divider></md-divider>\n            <md-list>\n              <h3 md-subheader>Features</h3>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n            </md-list>\n          </md-card>\n        </div>\n\n        <div flex-gt-xs=\"25\">\n          <md-card>\n            <md-toolbar color=\"accent\" class=\"bgc-light-blue-800 push-bottom\">\n              <span class=\"text-upper\">Plan Name</span>\n            </md-toolbar>\n            <md-card-subtitle>Plan description goes here.</md-card-subtitle>\n            <md-divider></md-divider>\n            <md-list>\n              <h3 md-subheader>Features</h3>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n            </md-list>\n          </md-card>\n        </div>\n\n        <div flex-gt-xs=\"25\">\n          <md-card>\n            <md-toolbar color=\"accent\" class=\"bgc-orange-700 push-bottom\">\n              <span class=\"text-upper\">Plan Name</span>\n            </md-toolbar>\n            <md-card-subtitle>Plan description goes here.</md-card-subtitle>\n            <md-divider></md-divider>\n            <md-list>\n              <h3 md-subheader>Features</h3>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n            </md-list>\n          </md-card>\n        </div>\n        <div flex-gt-xs=\"25\">\n          <md-card>\n            <md-toolbar color=\"accent\" class=\"bgc-deep-orange-700 push-bottom\">\n              <span class=\"text-upper\">Plan Name</span>\n            </md-toolbar>\n            <md-card-subtitle>Plan description goes here.</md-card-subtitle>\n            <md-divider></md-divider>\n            <md-list>\n              <h3 md-subheader>Features</h3>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n              <md-divider md-inset></md-divider>\n              <md-list-item>\n                  <md-icon md-list-icon class=\"fill-grey-700\">description</md-icon>\n                  <h4 md-line>Feature Name</h4>\n              </md-list-item>\n            </md-list>\n          </md-card>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"bgc-blue-600\">\n    <div layout-gt-xs=\"row\">\n      <div flex class=\"bg-cover\" style=\"background-image:url('https://assets.entrepreneur.com/static/1425478689-vince-vaughn-appearing-free-cheesy-stock-images-you-can-download-getty-2.jpg');background-size:cover;\">\n      </div>\n      <div flex class=\"pad\" tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['pad-xxl']\">\n        <div class=\"push-top-xxl\" hide-xs></div>\n        <h3 class=\"md-display-1 tc-white push-top-xs push-bottom-sm\">Contact Our Company</h3>\n        <p class=\"md-title tc-blue-50\">Ready to make dive into Angular 2? Clone this repo and get started today!</p>\n        <button md-raised-button color=\"accent\" class=\"push-bottom\">Contact Us</button>\n        <div class=\"push-top-xxl\" hide-xs></div>\n      </div>\n    </div>\n  </section>\n  <section class=\"bgc-blue-grey-800 tc-blue-grey-100\">\n    <div layout-gt-xs=\"row\" layout-align-gt-xs=\"center center\">\n      <div layout-gt-sm=\"row\" flex-gt-xs=\"75\" layout-margin layout-padding>\n        <div flex>\n          <md-nav-list>\n            <h3 md-subheader class=\"text-upper tc-blue-grey-100\">Menu Name</h3>\n            <md-list-item>\n              <p md-line md-caption class=\"md-body-2\">List Item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line >List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line >List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line >List item</p>\n            </md-list-item>\n          </md-nav-list>\n        </div>\n        <div flex>\n          <md-nav-list>\n            <h3 md-subheader class=\"text-upper tc-blue-grey-100\">Menu Name</h3>\n            <md-list-item>\n              <p md-line >List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line >List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line >List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line >List item</p>\n            </md-list-item>\n          </md-nav-list>\n        </div>\n        <div flex>\n          <md-nav-list>\n            <h3 md-subheader class=\"text-upper tc-blue-grey-100\">Menu Name</h3> \n            <md-list-item>\n              <p md-line>List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line>List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line>List item</p>\n            </md-list-item>\n            <md-list-item>\n              <p md-line>List item</p>\n            </md-list-item>\n          </md-nav-list>\n        </div>\n        <div flex-gt-xs=\"10\">\n        </div>\n        <div flex layout=\"column\" flex-gt-xs=\"30\" layout-align=\"right right\">\n          <md-icon push-right-xl svgIcon=\"assets:covalent\" class=\"md-icon-logo\"></md-icon>\n          <p class=\"text-sm\">&copy; Copyright 2016 Company Name, Inc. All rights reserved.</p>\n        </div>\n      </div>\n    </div>  \n  </section>\n</td-layout-nav>\n\n<a md-fab color=\"accent\" class=\"md-fab-position-bottom-right\" style=\"bottom:20px;\" href=\"https://github.com/Teradata/covalent-quickstart/tree/develop/src/app/templates\" target=\"_blank\"> \n  <md-icon>code</md-icon>\n</a>"

/***/ }),

/***/ 1035:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav logo=\"assets:covalent\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n    <span>Product Name</span>\n    <span flex></span>\n    <a md-icon-button mdTooltip=\"Docs\" href=\"https://teradata.github.io/covalent/\" target=\"_blank\"><md-icon>chrome_reader_mode</md-icon></a>\n    <a md-icon-button mdTooltip=\"Github\" href=\"https://github.com/teradata/covalent\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <td-layout-manage-list #manageList\n                        [opened]=\"media.registerQuery('gt-sm') | async\"\n                        [mode]=\"(media.registerQuery('gt-sm') | async) ? 'side' : 'push'\"\n                        [sidenavWidth]=\"(media.registerQuery('gt-xs') | async) ? '257px' : '100%'\">\n    <md-toolbar td-sidenav-content>\n      <span>Users</span>\n    </md-toolbar>\n    <md-nav-list td-sidenav-content (click)=\"!media.query('gt-sm') && manageList.close()\">\n      <a md-list-item>\n        <md-icon md-list-icon>account_circle</md-icon>\n        All Users\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>star</md-icon>\n        Favorites\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>verified_user</md-icon>\n        Administrators\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>supervisor_account</md-icon>\n        Non-Admins\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>inbox</md-icon>\n        Archived\n      </a>\n    </md-nav-list>\n    <div td-toolbar-content layout=\"row\" layout-align=\"start center\" flex>\n      <span class=\"text-capital\">{{action}} user</span>\n    </div>\n    <md-card tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['push']\">\n      <md-card-title>User Form</md-card-title>\n      <md-divider></md-divider>\n      <md-card-content class=\"push-bottom-none\">\n        <form #userForm=\"ngForm\">\n          <div layout=\"row\">\n            <md-input-container flex> \n              <input mdInput\n                      #displayNameElement\n                      #displayNameControl=\"ngModel\"\n                      type=\"text\" \n                      placeholder=\"Display Name\" \n                      [(ngModel)]=\"displayName\" \n                      name=\"displayName\" \n                      maxlength=\"20\" \n                      required>\n              <md-hint align=\"start\">\n                <span [hidden]=\"displayNameControl.pristine\" class=\"tc-red-600\">\n                  <span [hidden]=\"!displayNameControl.errors?.required\">Required</span>\n                </span>\n              </md-hint>\n              <md-hint align=\"end\">{{displayNameElement.value.length}} / 20</md-hint>\n            </md-input-container>\n          </div>\n          <div layout=\"row\" class=\"push-top\">\n            <md-input-container flex> \n              <input mdInput\n                      #emailElement\n                      #emailControl=\"ngModel\"\n                      type=\"text\" \n                      placeholder=\"Email\" \n                      [(ngModel)]=\"email\" \n                      name=\"email\"\n                      maxlength=\"30\"\n                      pattern=\"^[a-zA-Z0-9]+(\\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,15})$\"\n                      required>\n              <md-hint align=\"start\">\n                <span [hidden]=\"emailControl.pristine\" class=\"tc-red-600\">\n                  <span [hidden]=\"!emailControl.errors?.required\">Required</span>\n                  <span [hidden]=\"!emailControl.errors?.pattern\">incorrect pattern</span>\n                </span>\n              </md-hint>\n              <md-hint align=\"end\">{{emailElement.value.length}} / 30</md-hint>\n            </md-input-container>\n          </div>\n          <div layout=\"row\">\n            <md-slide-toggle [(ngModel)]=\"admin\" name=\"admin\">Admin</md-slide-toggle>\n          </div>\n        </form>\n      </md-card-content>\n      <md-divider></md-divider>\n      <md-card-actions>\n        <button md-button [disabled]=\"!userForm.form.valid\" color=\"primary\" (click)=\"save()\">SAVE</button>\n        <button md-button (click)=\"goBack()\">CANCEL</button>\n      </md-card-actions>\n    </md-card>\n  </td-layout-manage-list>\n</td-layout-nav>\n"

/***/ }),

/***/ 1036:
/***/ (function(module, exports) {

module.exports = "<td-layout-nav logo=\"assets:covalent\">\n  <div td-toolbar-content layout=\"row\" layout-align=\"center center\" flex>\n    <span>Product Name</span>\n    <span flex></span>\n    <a md-icon-button mdTooltip=\"Docs\" href=\"https://teradata.github.io/covalent/\" target=\"_blank\"><md-icon>chrome_reader_mode</md-icon></a>\n    <a md-icon-button mdTooltip=\"Github\" href=\"https://github.com/teradata/covalent\" target=\"_blank\"><md-icon svgIcon=\"assets:github\"></md-icon></a>\n  </div>\n  <td-layout-manage-list #manageList\n                        [opened]=\"media.registerQuery('gt-sm') | async\"\n                        [mode]=\"(media.registerQuery('gt-sm') | async) ? 'side' : 'push'\"\n                        [sidenavWidth]=\"(media.registerQuery('gt-xs') | async) ? '257px' : '100%'\">\n    <md-toolbar td-sidenav-content>\n      <span>Users</span>\n    </md-toolbar>\n    <md-nav-list td-sidenav-content (click)=\"!media.query('gt-sm') && manageList.close()\">\n      <a md-list-item>\n        <md-icon md-list-icon>account_circle</md-icon>\n        All Users\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>star</md-icon>\n        Favorites\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>verified_user</md-icon>\n        Administrators\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>supervisor_account</md-icon>\n        Non-Admins\n      </a>\n      <a md-list-item>\n        <md-icon md-list-icon>inbox</md-icon>\n        Archived\n      </a>\n    </md-nav-list>\n    <div td-toolbar-content layout=\"row\" layout-align=\"start center\" flex>\n      <span>All Users</span>\n      <span flex></span>\n      <button md-icon-button><md-icon class=\"md-24\">view_module</md-icon></button>\n      <button md-icon-button><md-icon class=\"md-24\">sort</md-icon></button>\n      <button md-icon-button><md-icon class=\"md-24\">settings</md-icon></button>\n      <button md-icon-button><md-icon class=\"md-24\">more_vert</md-icon></button>\n    </div>\n    <div class=\"md-content\">\n\n    </div>\n    <md-card tdMediaToggle=\"gt-xs\" [mediaClasses]=\"['push']\">\n      <td-search-box class=\"push-left push-right\" placeholder=\"search\" [alwaysVisible]=\"true\" (searchDebounce)=\"filterUsers($event)\"></td-search-box>\n      <md-divider></md-divider>\n      <ng-template tdLoading=\"users.list\">\n        <md-list class=\"will-load\">\n          <div class=\"md-padding\" *ngIf=\"!filteredUsers || filteredUsers.length === 0\" layout=\"row\" layout-align=\"center center\">\n            <h3>No users to display.</h3>\n          </div>\n          <ng-template let-user let-last=\"last\" ngFor [ngForOf]=\"filteredUsers\">\n            <md-list-item>\n              <md-icon md-list-avatar>person</md-icon>\n              <h3 md-line> {{user.displayName}} </h3>\n              <p md-line> {{user.email}} </p>\n              <p md-line hide-gt-md class=\"md-caption\"> last login: {{ user.lastAccess | timeAgo }} </p>\n              <span flex></span>\n              <span hide-xs hide-sm hide-md flex-gt-xs=\"60\" flex-xs=\"40\" layout-gt-xs=\"row\">\n                  <div class=\"md-caption tc-grey-500\" flex-gt-xs=\"50\"> {{ user.created | date }} </div>\n                  <div class=\"md-caption tc-grey-500\" flex-gt-xs=\"50\"> {{ user.lastAccess | timeAgo }} </div>\n              </span>\n              <span>\n                  <button md-icon-button [md-menu-trigger-for]=\"menu\">\n                  <md-icon>more_vert</md-icon>\n                  </button>\n                  <md-menu x-position=\"before\" #menu=\"mdMenu\">\n                      <a [routerLink]=\"[user.id + '/edit']\" md-menu-item>Edit</a>\n                      <button md-menu-item>Delete</button>\n                  </md-menu>\n              </span>\n            </md-list-item>\n            <md-divider *ngIf=\"!last\" md-inset></md-divider>\n          </ng-template>\n        </md-list>\n      </ng-template>\n    </md-card>\n  </td-layout-manage-list>   \n</td-layout-nav>\n<a md-fab color=\"accent\" class=\"mat-fab-bottom-right\" [routerLink]=\"['add']\">\n  <md-icon>add</md-icon>\n</a>\n"

/***/ }),

/***/ 1094:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(374);


/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MOCK_API; });
var MOCK_API = 'http://localhost:8080';
//# sourceMappingURL=api.config.js.map

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutComponent = (function () {
    function AboutComponent(_router, elementRef) {
        this._router = _router;
        this.elementRef = elementRef;
    }
    AboutComponent.prototype.ngAfterViewInit = function () {
        var jQueryScript = document.createElement("script");
        jQueryScript.type = "text/javascript";
        jQueryScript.src = "https://code.jquery.com/jquery-3.2.1.min.js";
        jQueryScript.integrity = "sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=";
        jQueryScript.crossOrigin = "anonymous";
        this.elementRef.nativeElement.appendChild(jQueryScript);
        var menuScript = document.createElement("script");
        menuScript.type = "text/javascript";
        menuScript.src = "assets/worlddata/animateMenu.js";
        this.elementRef.nativeElement.appendChild(menuScript);
        var googleAnalyticsScript = document.createElement("script");
        googleAnalyticsScript.type = "text/javascript";
        googleAnalyticsScript.src = "assets/worlddata/googleanalytics.js";
        this.elementRef.nativeElement.appendChild(googleAnalyticsScript);
    };
    AboutComponent.prototype.goBack = function () {
        this._router.navigate(['/']);
    };
    return AboutComponent;
}());
AboutComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'about',
        template: __webpack_require__(1018),
        styles: [__webpack_require__(821)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object])
], AboutComponent);

var _a, _b;
//# sourceMappingURL=about.component.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__covalent_core__ = __webpack_require__(20);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardProductComponent = (function () {
    function DashboardProductComponent(_titleService, media) {
        this._titleService = _titleService;
        this.media = media;
    }
    DashboardProductComponent.prototype.ngAfterViewInit = function () {
        // broadcast to all listener observables when loading the page
        this.media.broadcast();
        this._titleService.setTitle('Product Dashboard');
        this.title = this._titleService.getTitle();
    };
    return DashboardProductComponent;
}());
DashboardProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-dashboard-product',
        template: __webpack_require__(1020),
        styles: [__webpack_require__(823)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["d" /* TdMediaService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["d" /* TdMediaService */]) === "function" && _b || Object])
], DashboardProductComponent);

var _a, _b;
//# sourceMappingURL=dashboard-product.component.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductFeaturesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductFeaturesComponent = (function () {
    function ProductFeaturesComponent(_titleService, _dialogService, _featuresService, _loadingService) {
        this._titleService = _titleService;
        this._dialogService = _dialogService;
        this._featuresService = _featuresService;
        this._loadingService = _loadingService;
    }
    ProductFeaturesComponent.prototype.openConfirm = function (id) {
        var _this = this;
        this._dialogService.openConfirm({
            message: 'Are you sure you want to delete this feature? It\'s being used!',
            title: 'Confirm',
            cancelButton: 'No, Cancel',
            acceptButton: 'Yes, Delete',
        }).afterClosed().subscribe(function (accept) {
            if (accept) {
                _this.deleteFeature(id);
            }
            else {
                // DO SOMETHING ELSE
            }
        });
    };
    ProductFeaturesComponent.prototype.ngAfterViewInit = function () {
        this._titleService.setTitle('Product Features');
        this.loadFeatures();
    };
    ProductFeaturesComponent.prototype.filterFeatures = function (filterTitle) {
        if (filterTitle === void 0) { filterTitle = ''; }
        this.filteredFeatures = this.features.filter(function (feature) {
            return feature.title.toLowerCase().indexOf(filterTitle.toLowerCase()) > -1;
        });
    };
    ProductFeaturesComponent.prototype.loadFeatures = function () {
        var _this = this;
        this._loadingService.register('features.list');
        this._featuresService.query().subscribe(function (features) {
            _this.features = features;
            _this.filteredFeatures = features;
            _this._loadingService.resolve('features.list');
        }, function (error) {
            _this._featuresService.staticQuery().subscribe(function (features) {
                _this.features = features;
                _this.filteredFeatures = features;
                _this._loadingService.resolve('features.list');
            });
        });
    };
    ProductFeaturesComponent.prototype.deleteFeature = function (id) {
        var _this = this;
        this._loadingService.register('features.list');
        this._featuresService.delete(id).subscribe(function () {
            _this.features = _this.features.filter(function (feature) {
                return feature.id !== id;
            });
            _this.filteredFeatures = _this.filteredFeatures.filter(function (feature) {
                return feature.id !== id;
            });
            _this._loadingService.resolve('features.list');
        }, function (error) {
            _this._loadingService.resolve('features.list');
        });
    };
    return ProductFeaturesComponent;
}());
ProductFeaturesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-product-features',
        template: __webpack_require__(1021),
        styles: [__webpack_require__(824)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_3__services__["d" /* FeaturesService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["g" /* TdDialogService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["g" /* TdDialogService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["d" /* FeaturesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["d" /* FeaturesService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["f" /* TdLoadingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["f" /* TdLoadingService */]) === "function" && _d || Object])
], ProductFeaturesComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=features.component.js.map

/***/ }),

/***/ 246:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeaturesFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeaturesFormComponent = (function () {
    function FeaturesFormComponent(_featuresService, _route) {
        this._featuresService = _featuresService;
        this._route = _route;
    }
    FeaturesFormComponent.prototype.goBack = function () {
        window.history.back();
    };
    FeaturesFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.url.subscribe(function (url) {
            _this.action = (url.length > 1 ? url[1].path : 'add');
        });
        this._route.params.subscribe(function (params) {
            var featureId = params.id;
            _this._featuresService.get(featureId).subscribe(function (feature) {
                _this.title = feature.title;
                _this.user = feature.user;
                _this.enabled = (feature.enabled === 1 ? true : false);
                _this.id = feature.id;
            });
        });
    };
    FeaturesFormComponent.prototype.save = function () {
        var _this = this;
        var enabled = (this.enabled ? 1 : 0);
        var now = new Date();
        this.feature = {
            title: this.title,
            user: this.user,
            enabled: enabled,
            icon: this.icon,
            id: this.id || this.title.replace(/\s+/g, '.'),
            created: now,
            modified: now,
        };
        if (this.action === 'add') {
            this._featuresService.create(this.feature).subscribe(function () {
                _this.goBack();
            });
        }
        else {
            this._featuresService.update(this.id, this.feature).subscribe(function () {
                _this.goBack();
            });
        }
    };
    return FeaturesFormComponent;
}());
FeaturesFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-feature-form',
        template: __webpack_require__(1022),
        styles: [__webpack_require__(825)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_2__services__["d" /* FeaturesService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* FeaturesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* FeaturesService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* ActivatedRoute */]) === "function" && _b || Object])
], FeaturesFormComponent);

var _a, _b;
//# sourceMappingURL=form.component.js.map

/***/ }),

/***/ 247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductOverviewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductOverviewComponent = (function () {
    function ProductOverviewComponent(_titleService, _itemsService, _usersService, _loadingService) {
        this._titleService = _titleService;
        this._itemsService = _itemsService;
        this._usersService = _usersService;
        this._loadingService = _loadingService;
        // Generic Chart options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = true;
        this.autoScale = true;
        this.showLegend = false;
        this.showXAxisLabel = false;
        this.showYAxisLabel = false;
        this.xAxisLabel = 'X Axis';
        this.yAxisLabel = 'Y Axis';
        this.orangeColorScheme = {
            domain: [
                '#E64A19', '#F57C00', '#FFA726', '#FFB74D', '#FFCC80',
            ],
        };
        this.blueColorScheme = {
            domain: [
                '#01579B', '#00B0FF', '#80D8FF', '#E1F5FE',
            ],
        };
        // Chart Single
        Object.assign(this, { single: __WEBPACK_IMPORTED_MODULE_2__data__["a" /* single */] });
        // Chart Multi
        this.multi = __WEBPACK_IMPORTED_MODULE_2__data__["b" /* multi */].map(function (group) {
            group.series = group.series.map(function (dataItem) {
                dataItem.name = new Date(dataItem.name);
                return dataItem;
            });
            return group;
        });
        // Chart Multi2
        this.multi2 = __WEBPACK_IMPORTED_MODULE_2__data__["c" /* multi2 */].map(function (group) {
            group.series = group.series.map(function (dataItem) {
                dataItem.name = new Date(dataItem.name);
                return dataItem;
            });
            return group;
        });
    }
    ProductOverviewComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this._titleService.setTitle('Product Name');
        this._loadingService.register('items.load');
        this._itemsService.query().subscribe(function (items) {
            _this.items = items;
            setTimeout(function () {
                _this._loadingService.resolve('items.load');
            }, 2000);
        }, function (error) {
            _this._itemsService.staticQuery().subscribe(function (items) {
                _this.items = items;
                setTimeout(function () {
                    _this._loadingService.resolve('items.load');
                }, 2000);
            });
        });
        this._loadingService.register('users.load');
        this._usersService.query().subscribe(function (users) {
            _this.users = users;
            setTimeout(function () {
                _this._loadingService.resolve('users.load');
            }, 2000);
        }, function (error) {
            _this._usersService.staticQuery().subscribe(function (users) {
                _this.users = users;
                setTimeout(function () {
                    _this._loadingService.resolve('users.load');
                }, 2000);
            });
        });
    };
    // ngx transform using covalent digits pipe
    ProductOverviewComponent.prototype.axisDigits = function (val) {
        return new __WEBPACK_IMPORTED_MODULE_3__covalent_core__["h" /* TdDigitsPipe */]().transform(val);
    };
    return ProductOverviewComponent;
}());
ProductOverviewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-product-overview',
        template: __webpack_require__(1023),
        styles: [__webpack_require__(826)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_4__services__["a" /* ItemsService */], __WEBPACK_IMPORTED_MODULE_4__services__["b" /* UsersService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services__["a" /* ItemsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services__["a" /* ItemsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services__["b" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services__["b" /* UsersService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__covalent_core__["f" /* TdLoadingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__covalent_core__["f" /* TdLoadingService */]) === "function" && _d || Object])
], ProductOverviewComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=overview.component.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__covalent_core__ = __webpack_require__(20);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductStatsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NUMBER_FORMAT = function (v) { return v.value; };
var DECIMAL_FORMAT = function (v) { return v.value.toFixed(2); };
var ProductStatsComponent = (function () {
    function ProductStatsComponent(_titleService, _dataTableService) {
        this._titleService = _titleService;
        this._dataTableService = _dataTableService;
        this.columns = [
            { name: 'name', label: 'Product' },
            { name: 'type', label: 'Type' },
            { name: 'usage', label: 'CPU Time (m)', numeric: true, format: NUMBER_FORMAT },
            { name: 'users', label: 'Users (K)', numeric: true, format: DECIMAL_FORMAT },
            { name: 'load', label: 'load (%)', numeric: true, format: NUMBER_FORMAT },
            { name: 'time', label: 'time (h)', numeric: true, format: DECIMAL_FORMAT },
            { name: 'quota', label: 'Quota (%)', numeric: true, format: NUMBER_FORMAT },
            { name: 'sessions', label: 'Sessions', numeric: true, format: NUMBER_FORMAT },
            { name: 'containers', label: 'Containers', numeric: true, format: NUMBER_FORMAT },
        ];
        this.data = [
            {
                'name': 'Ingest',
                'type': 'container',
                'usage': { 'value': 159.0 },
                'users': { 'value': 6.0 },
                'load': { 'value': 24.0 },
                'time': { 'value': 4.0 },
                'quota': { 'value': 87.0 },
                'sessions': { 'value': 14.0 },
                'containers': { 'value': 1.0 },
            }, {
                'name': 'Containers',
                'type': 'container',
                'usage': { 'value': 237.0 },
                'users': { 'value': 9.0 },
                'load': { 'value': 37.0 },
                'time': { 'value': 4.3 },
                'quota': { 'value': 129.0 },
                'sessions': { 'value': 8.0 },
                'containers': { 'value': 1.0 },
            }, {
                'name': 'Computer Engines',
                'type': 'hardware',
                'usage': { 'value': 262.0 },
                'users': { 'value': 16.0 },
                'load': { 'value': 24.0 },
                'time': { 'value': 6.0 },
                'quota': { 'value': 337.0 },
                'sessions': { 'value': 6.0 },
                'containers': { 'value': 7.0 },
            }, {
                'name': 'Memory',
                'type': 'hardware',
                'usage': { 'value': 305.0 },
                'users': { 'value': 3.7 },
                'load': { 'value': 67.0 },
                'time': { 'value': 4.3 },
                'quota': { 'value': 413.0 },
                'sessions': { 'value': 3.0 },
                'containers': { 'value': 8.0 },
            }, {
                'name': 'Workload Engine',
                'type': 'engines',
                'usage': { 'value': 375.0 },
                'users': { 'value': 0.0 },
                'load': { 'value': 94.0 },
                'time': { 'value': 0.0 },
                'quota': { 'value': 50.0 },
                'sessions': { 'value': 0.0 },
                'containers': { 'value': 0.0 },
            }, {
                'name': 'High Availability',
                'type': 'container',
                'usage': { 'value': 392.0 },
                'users': { 'value': 0.2 },
                'load': { 'value': 98.0 },
                'time': { 'value': 0.0 },
                'quota': { 'value': 38.0 },
                'sessions': { 'value': 0.0 },
                'containers': { 'value': 2.0 },
            }, {
                'name': 'Database',
                'type': 'engines',
                'usage': { 'value': 408.0 },
                'users': { 'value': 3.2 },
                'load': { 'value': 87.0 },
                'time': { 'value': 6.5 },
                'quota': { 'value': 562.0 },
                'sessions': { 'value': 0.0 },
                'containers': { 'value': 45.0 },
            }, {
                'name': 'Logs',
                'type': 'containers',
                'usage': { 'value': 452.0 },
                'users': { 'value': 25.0 },
                'load': { 'value': 51.0 },
                'time': { 'value': 4.9 },
                'quota': { 'value': 326.0 },
                'sessions': { 'value': 2.0 },
                'containers': { 'value': 22.0 },
            }, {
                'name': 'Orchestrator',
                'type': 'service',
                'usage': { 'value': 518.0 },
                'users': { 'value': 26.0 },
                'load': { 'value': 65.0 },
                'time': { 'value': 7.0 },
                'quota': { 'value': 54.0 },
                'sessions': { 'value': 12.0 },
                'containers': { 'value': 6.0 },
            },
        ];
        // Generic Chart options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = true;
        this.autoScale = true;
        this.showLegend = false;
        this.showXAxisLabel = false;
        this.showYAxisLabel = false;
        this.xAxisLabel = 'X Axis';
        this.yAxisLabel = 'Y Axis';
        this.colorScheme = {
            domain: [
                '#01579B', '#0091EA', '#FFB74D', '#E64A19',
            ],
        };
        this.filteredData = this.data;
        this.filteredTotal = this.data.length;
        this.searchTerm = '';
        this.fromRow = 1;
        this.currentPage = 1;
        this.pageSize = 5;
        this.sortBy = 'name';
        this.sortOrder = __WEBPACK_IMPORTED_MODULE_3__covalent_core__["b" /* TdDataTableSortingOrder */].Descending;
        // Chart Single
        Object.assign(this, { single: __WEBPACK_IMPORTED_MODULE_2__data__["a" /* single */] });
        // Chart Multi
        this.multi = __WEBPACK_IMPORTED_MODULE_2__data__["b" /* multi */].map(function (group) {
            group.series = group.series.map(function (dataItem) {
                dataItem.name = new Date(dataItem.name);
                return dataItem;
            });
            return group;
        });
    }
    ProductStatsComponent.prototype.ngAfterViewInit = function () {
        this._titleService.setTitle('Product Stats');
        this.filter();
    };
    ProductStatsComponent.prototype.sort = function (sortEvent) {
        this.sortBy = sortEvent.name;
        this.sortOrder = sortEvent.order;
        this.filter();
    };
    ProductStatsComponent.prototype.search = function (searchTerm) {
        this.searchTerm = searchTerm;
        this.filter();
    };
    ProductStatsComponent.prototype.page = function (pagingEvent) {
        this.fromRow = pagingEvent.fromRow;
        this.currentPage = pagingEvent.page;
        this.pageSize = pagingEvent.pageSize;
        this.filter();
    };
    ProductStatsComponent.prototype.filter = function () {
        var newData = this.data;
        newData = this._dataTableService.filterData(newData, this.searchTerm, true);
        this.filteredTotal = newData.length;
        newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
        newData = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
        this.filteredData = newData;
    };
    // ngx transform using covalent digits pipe
    ProductStatsComponent.prototype.axisDigits = function (val) {
        return new __WEBPACK_IMPORTED_MODULE_3__covalent_core__["h" /* TdDigitsPipe */]().transform(val);
    };
    return ProductStatsComponent;
}());
ProductStatsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-product-stats',
        template: __webpack_require__(1024),
        styles: [__webpack_require__(827)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__covalent_core__["c" /* TdDataTableService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__covalent_core__["c" /* TdDataTableService */]) === "function" && _b || Object])
], ProductStatsComponent);

var _a, _b;
//# sourceMappingURL=stats.component.js.map

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailComponent = (function () {
    function DetailComponent(_router, _itemsService, _route) {
        this._router = _router;
        this._itemsService = _itemsService;
        this._route = _route;
        this.item = {};
    }
    DetailComponent.prototype.goBack = function () {
        this._router.navigate(['/product']);
    };
    DetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.params.subscribe(function (params) {
            var itemId = params.id;
            _this._itemsService.get(itemId).subscribe(function (item) {
                _this.item = item;
            }, function (error) {
                _this._itemsService.staticGet(itemId).subscribe(function (item) {
                    _this.item = item;
                });
            });
        });
    };
    return DetailComponent;
}());
DetailComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-detail',
        template: __webpack_require__(1026),
        styles: [__webpack_require__(829)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_2__services__["a" /* ItemsService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services__["a" /* ItemsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["a" /* ItemsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* ActivatedRoute */]) === "function" && _c || Object])
], DetailComponent);

var _a, _b, _c;
//# sourceMappingURL=detail.component.js.map

/***/ }),

/***/ 250:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__covalent_core__ = __webpack_require__(20);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FormComponent = (function () {
    function FormComponent(media) {
        this.media = media;
        this.activeDeactiveStep1Msg = 'No select/deselect detected yet';
        this.stateStep2 = __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].Required;
        this.stateStep3 = __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].Complete;
        this.disabled = false;
    }
    FormComponent.prototype.ngAfterViewInit = function () {
        // broadcast to all listener observables when loading the page
        this.media.broadcast();
    };
    FormComponent.prototype.toggleRequiredStep2 = function () {
        this.stateStep2 = (this.stateStep2 === __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].Required ? __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].None : __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].Required);
    };
    FormComponent.prototype.toggleCompleteStep3 = function () {
        this.stateStep3 = (this.stateStep3 === __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].Complete ? __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].None : __WEBPACK_IMPORTED_MODULE_1__covalent_core__["e" /* StepState */].Complete);
    };
    FormComponent.prototype.toggleDisabled = function () {
        this.disabled = !this.disabled;
    };
    FormComponent.prototype.activeStep1Event = function () {
        this.activeDeactiveStep1Msg = 'Active event emitted.';
    };
    FormComponent.prototype.deactiveStep1Event = function () {
        this.activeDeactiveStep1Msg = 'Deactive event emitted.';
    };
    return FormComponent;
}());
FormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-product-form',
        template: __webpack_require__(1027),
        styles: [__webpack_require__(830)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__covalent_core__["d" /* TdMediaService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__covalent_core__["d" /* TdMediaService */]) === "function" && _a || Object])
], FormComponent);

var _a;
//# sourceMappingURL=form.component.js.map

/***/ }),

/***/ 251:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_trends_service__ = __webpack_require__(260);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { ItemsService, UsersService, ProductsService, AlertsService } from '../../services';
//import {MdSlideToggleModule} from '@angular/material';
//import * as world from './data/world-110.json';
//import * as countryNames from './data/world-country-names.tsv';
var GlobeComponent = (function () {
    function GlobeComponent(elementRef, trendsService, _router) {
        this.elementRef = elementRef;
        this.trendsService = trendsService;
        this._router = _router;
        this.trends = [];
        this.routes = [{
                title: 'Globe',
                route: '/',
            }, {
                title: 'About',
                route: '/about',
            },
        ];
    }
    ;
    GlobeComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Retrieve daily trends from API
        this.trendsService.getAllTrends().subscribe(function (trends) {
            _this.trends = trends;
        });
    };
    GlobeComponent.prototype.ngAfterViewInit = function () {
        var d3Script = document.createElement("script");
        d3Script.type = "text/javascript";
        d3Script.src = "//d3js.org/d3.v3.min.js";
        this.elementRef.nativeElement.appendChild(d3Script);
        var queueScript = document.createElement("script");
        queueScript.type = "text/javascript";
        queueScript.src = "//d3js.org/queue.v1.min.js";
        this.elementRef.nativeElement.appendChild(queueScript);
        var topojsonScript = document.createElement("script");
        topojsonScript.type = "text/javascript";
        topojsonScript.src = "//d3js.org/topojson.v1.min.js";
        this.elementRef.nativeElement.appendChild(topojsonScript);
        var socketioScript = document.createElement("script");
        socketioScript.type = "text/javascript";
        socketioScript.src = "https://cdn.socket.io/socket.io-1.4.5.js";
        this.elementRef.nativeElement.appendChild(socketioScript);
        var planetaryScript = document.createElement("script");
        planetaryScript.type = "text/javascript";
        planetaryScript.src = "assets/worlddata/planetaryjs.min.js";
        this.elementRef.nativeElement.appendChild(planetaryScript);
        var jQueryScript = document.createElement("script");
        jQueryScript.type = "text/javascript";
        jQueryScript.src = "https://code.jquery.com/jquery-3.2.1.min.js";
        jQueryScript.integrity = "sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=";
        jQueryScript.crossOrigin = "anonymous";
        this.elementRef.nativeElement.appendChild(jQueryScript);
        var bootstrapScript = document.createElement("script");
        bootstrapScript.type = "text/javascript";
        bootstrapScript.src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js";
        bootstrapScript.integrity = "sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa";
        bootstrapScript.crossOrigin = "anonymous";
        this.elementRef.nativeElement.appendChild(bootstrapScript);
        var globeScript = document.createElement("script");
        globeScript.type = "text/javascript";
        globeScript.src = "assets/worlddata/globe.js";
        this.elementRef.nativeElement.appendChild(globeScript);
        var googleAnalyticsScript = document.createElement("script");
        googleAnalyticsScript.type = "text/javascript";
        googleAnalyticsScript.src = "assets/worlddata/googleanalytics.js";
        this.elementRef.nativeElement.appendChild(googleAnalyticsScript);
    };
    GlobeComponent.prototype.goToAbout = function () {
        this._router.navigate(['/about']);
    };
    return GlobeComponent;
}());
GlobeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-dashboard',
        template: __webpack_require__(1028),
        styles: [__webpack_require__(831)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_trends_service__["a" /* TrendsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_trends_service__["a" /* TrendsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _c || Object])
], GlobeComponent);

var _a, _b, _c;
//# sourceMappingURL=globe.component.js.map

/***/ }),

/***/ 252:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LogsComponent = (function () {
    function LogsComponent(_titleService, _itemsService, _usersService, _productsService, _loadingService, media) {
        this._titleService = _titleService;
        this._itemsService = _itemsService;
        this._usersService = _usersService;
        this._productsService = _productsService;
        this._loadingService = _loadingService;
        this.media = media;
    }
    LogsComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // broadcast to all listener observables when loading the page
        this.media.broadcast();
        this._titleService.setTitle('Covalent Logs');
        this._loadingService.register('items.load');
        this._itemsService.query().subscribe(function (items) {
            _this.items = items;
            setTimeout(function () {
                _this._loadingService.resolve('items.load');
            }, 2000);
        }, function (error) {
            _this._itemsService.staticQuery().subscribe(function (items) {
                _this.items = items;
                setTimeout(function () {
                    _this._loadingService.resolve('items.load');
                }, 2000);
            });
        });
        this._loadingService.register('products.load');
        this._productsService.query().subscribe(function (products) {
            _this.products = products;
            setTimeout(function () {
                _this._loadingService.resolve('products.load');
            }, 2000);
        });
        this._loadingService.register('users.load');
        this._usersService.query().subscribe(function (users) {
            _this.users = users;
            setTimeout(function () {
                _this._loadingService.resolve('users.load');
            }, 2000);
        }, function (error) {
            _this._usersService.staticQuery().subscribe(function (users) {
                _this.users = users;
                setTimeout(function () {
                    _this._loadingService.resolve('users.load');
                }, 2000);
            });
        });
    };
    return LogsComponent;
}());
LogsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-logs',
        template: __webpack_require__(1029),
        styles: [__webpack_require__(832)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_3__services__["a" /* ItemsService */], __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */], __WEBPACK_IMPORTED_MODULE_3__services__["c" /* ProductsService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services__["a" /* ItemsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["a" /* ItemsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services__["c" /* ProductsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["c" /* ProductsService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["f" /* TdLoadingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["f" /* TdLoadingService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["d" /* TdMediaService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["d" /* TdMediaService */]) === "function" && _f || Object])
], LogsComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=logs.component.js.map

/***/ }),

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MainComponent = (function () {
    function MainComponent() {
    }
    return MainComponent;
}());
MainComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-main',
        template: __webpack_require__(1030),
        styles: [__webpack_require__(833)],
    })
], MainComponent);

//# sourceMappingURL=main.component.js.map

/***/ }),

/***/ 254:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data__ = __webpack_require__(436);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardTemplateComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardTemplateComponent = (function () {
    function DashboardTemplateComponent() {
        this.view = [700, 400];
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = false;
        this.showXAxisLabel = true;
        this.xAxisLabel = '';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Sales';
        this.colorScheme = {
            domain: ['#9575CD', '#4FC3F7', '#4DD0E1', '#4DB6AC', '#66BB6A', '#9CCC65'],
        };
        this.colorSchemeDark = {
            domain: ['#5E35B1', '#0277BD', '#00695C', '#558B2F', '#9E9D24'],
        };
        // line, area
        this.autoScale = true;
        // Cards
        Object.assign(this, { single: __WEBPACK_IMPORTED_MODULE_1__data__["a" /* single */] });
        // Chart
        this.multi = __WEBPACK_IMPORTED_MODULE_1__data__["b" /* multi */].map(function (group) {
            group.series = group.series.map(function (dataItem) {
                dataItem.name = new Date(dataItem.name);
                return dataItem;
            });
            return group;
        });
    }
    return DashboardTemplateComponent;
}());
DashboardTemplateComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-dashboard',
        template: __webpack_require__(1031),
        styles: [__webpack_require__(834)],
    }),
    __metadata("design:paramtypes", [])
], DashboardTemplateComponent);

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ 255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data__ = __webpack_require__(437);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditorTemplateComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NUMBER_FORMAT = function (v) { return v.value; };
var DECIMAL_FORMAT = function (v) { return v.value.toFixed(2); };

var EditorTemplateComponent = (function () {
    function EditorTemplateComponent(_titleService, _dataTableService) {
        this._titleService = _titleService;
        this._dataTableService = _dataTableService;
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = false;
        this.showXAxisLabel = true;
        this.xAxisLabel = '';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Sales';
        this.autoScale = true;
        this.colorScheme = {
            domain: ['#9575CD', '#4FC3F7', '#4DD0E1', '#4DB6AC', '#66BB6A', '#9CCC65'],
        };
        // Datatable
        this.columns = [
            { name: 'name', label: 'Product' },
            { name: 'type', label: 'Type' },
            { name: 'usage', label: 'CPU Time (m)', numeric: true, format: NUMBER_FORMAT },
            { name: 'users', label: 'Users (K)', numeric: true, format: DECIMAL_FORMAT },
            { name: 'load', label: 'load (%)', numeric: true, format: NUMBER_FORMAT },
            { name: 'time', label: 'time (h)', numeric: true, format: DECIMAL_FORMAT },
            { name: 'quota', label: 'Quota (%)', numeric: true, format: NUMBER_FORMAT },
            { name: 'sessions', label: 'Sessions', numeric: true, format: NUMBER_FORMAT },
            { name: 'containers', label: 'Containers', numeric: true, format: NUMBER_FORMAT },
        ];
        this.data = [
            {
                'name': 'Ingest',
                'type': 'container',
                'usage': { 'value': 159.0 },
                'users': { 'value': 6.0 },
                'load': { 'value': 24.0 },
                'time': { 'value': 4.0 },
                'quota': { 'value': 87.0 },
                'sessions': { 'value': 14.0 },
                'containers': { 'value': 1.0 },
            }, {
                'name': 'Containers',
                'type': 'container',
                'usage': { 'value': 237.0 },
                'users': { 'value': 9.0 },
                'load': { 'value': 37.0 },
                'time': { 'value': 4.3 },
                'quota': { 'value': 129.0 },
                'sessions': { 'value': 8.0 },
                'containers': { 'value': 1.0 },
            }, {
                'name': 'Computer Engines',
                'type': 'hardware',
                'usage': { 'value': 262.0 },
                'users': { 'value': 16.0 },
                'load': { 'value': 24.0 },
                'time': { 'value': 6.0 },
                'quota': { 'value': 337.0 },
                'sessions': { 'value': 6.0 },
                'containers': { 'value': 7.0 },
            }, {
                'name': 'Memory',
                'type': 'hardware',
                'usage': { 'value': 305.0 },
                'users': { 'value': 3.7 },
                'load': { 'value': 67.0 },
                'time': { 'value': 4.3 },
                'quota': { 'value': 413.0 },
                'sessions': { 'value': 3.0 },
                'containers': { 'value': 8.0 },
            }, {
                'name': 'Workload Engine',
                'type': 'engines',
                'usage': { 'value': 375.0 },
                'users': { 'value': 0.0 },
                'load': { 'value': 94.0 },
                'time': { 'value': 0.0 },
                'quota': { 'value': 50.0 },
                'sessions': { 'value': 0.0 },
                'containers': { 'value': 0.0 },
            }, {
                'name': 'High Availability',
                'type': 'container',
                'usage': { 'value': 392.0 },
                'users': { 'value': 0.2 },
                'load': { 'value': 98.0 },
                'time': { 'value': 0.0 },
                'quota': { 'value': 38.0 },
                'sessions': { 'value': 0.0 },
                'containers': { 'value': 2.0 },
            }, {
                'name': 'Database',
                'type': 'engines',
                'usage': { 'value': 408.0 },
                'users': { 'value': 3.2 },
                'load': { 'value': 87.0 },
                'time': { 'value': 6.5 },
                'quota': { 'value': 562.0 },
                'sessions': { 'value': 0.0 },
                'containers': { 'value': 45.0 },
            }, {
                'name': 'Logs',
                'type': 'containers',
                'usage': { 'value': 452.0 },
                'users': { 'value': 25.0 },
                'load': { 'value': 51.0 },
                'time': { 'value': 4.9 },
                'quota': { 'value': 326.0 },
                'sessions': { 'value': 2.0 },
                'containers': { 'value': 22.0 },
            }, {
                'name': 'Orchestrator',
                'type': 'service',
                'usage': { 'value': 518.0 },
                'users': { 'value': 26.0 },
                'load': { 'value': 65.0 },
                'time': { 'value': 7.0 },
                'quota': { 'value': 54.0 },
                'sessions': { 'value': 12.0 },
                'containers': { 'value': 6.0 },
            },
        ];
        this.filteredData = this.data;
        this.filteredTotal = this.data.length;
        this.searchTerm = '';
        this.fromRow = 1;
        this.currentPage = 1;
        this.pageSize = 5;
        this.sortBy = 'name';
        this.sortOrder = __WEBPACK_IMPORTED_MODULE_2__covalent_core__["b" /* TdDataTableSortingOrder */].Descending;
        // Chart
        this.multi = __WEBPACK_IMPORTED_MODULE_3__data__["a" /* multi */].map(function (group) {
            group.series = group.series.map(function (dataItem) {
                dataItem.name = new Date(dataItem.name);
                return dataItem;
            });
            return group;
        });
    }
    EditorTemplateComponent.prototype.ngAfterViewInit = function () {
        this._titleService.setTitle('Teradata Studio Express 2017');
        this.filter();
    };
    EditorTemplateComponent.prototype.sort = function (sortEvent) {
        this.sortBy = sortEvent.name;
        this.sortOrder = sortEvent.order;
        this.filter();
    };
    EditorTemplateComponent.prototype.search = function (searchTerm) {
        this.searchTerm = searchTerm;
        this.filter();
    };
    EditorTemplateComponent.prototype.page = function (pagingEvent) {
        this.fromRow = pagingEvent.fromRow;
        this.currentPage = pagingEvent.page;
        this.pageSize = pagingEvent.pageSize;
        this.filter();
    };
    EditorTemplateComponent.prototype.filter = function () {
        var newData = this.data;
        newData = this._dataTableService.filterData(newData, this.searchTerm, true);
        this.filteredTotal = newData.length;
        newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
        newData = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
        this.filteredData = newData;
    };
    return EditorTemplateComponent;
}());
EditorTemplateComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-editor',
        template: __webpack_require__(1032),
        styles: [__webpack_require__(835)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["c" /* TdDataTableService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["c" /* TdDataTableService */]) === "function" && _b || Object])
], EditorTemplateComponent);

var _a, _b;
//# sourceMappingURL=editor.component.js.map

/***/ }),

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__covalent_core__ = __webpack_require__(20);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailTemplateComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmailTemplateComponent = (function () {
    function EmailTemplateComponent(media) {
        this.media = media;
    }
    EmailTemplateComponent.prototype.ngAfterViewInit = function () {
        // broadcast to all listener observables when loading the page
        this.media.broadcast();
    };
    return EmailTemplateComponent;
}());
EmailTemplateComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-email',
        template: __webpack_require__(1033),
        styles: [__webpack_require__(836)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__covalent_core__["d" /* TdMediaService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__covalent_core__["d" /* TdMediaService */]) === "function" && _a || Object])
], EmailTemplateComponent);

var _a;
//# sourceMappingURL=email.component.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TemplatesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TemplatesComponent = (function () {
    function TemplatesComponent() {
    }
    return TemplatesComponent;
}());
TemplatesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-templates',
        template: __webpack_require__(1034),
        styles: [__webpack_require__(837)],
    })
], TemplatesComponent);

//# sourceMappingURL=templates.component.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsersFormComponent = (function () {
    function UsersFormComponent(_usersService, _route, media) {
        this._usersService = _usersService;
        this._route = _route;
        this.media = media;
    }
    UsersFormComponent.prototype.goBack = function () {
        window.history.back();
    };
    UsersFormComponent.prototype.ngAfterViewInit = function () {
        // broadcast to all listener observables when loading the page
        this.media.broadcast();
    };
    UsersFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.url.subscribe(function (url) {
            _this.action = (url.length > 1 ? url[1].path : 'add');
        });
        this._route.params.subscribe(function (params) {
            var userId = params.id;
            _this._usersService.get(userId).subscribe(function (user) {
                _this.displayName = user.displayName;
                _this.email = user.email;
                _this.admin = (user.siteAdmin === 1 ? true : false);
                _this.id = user.id;
            });
        });
    };
    UsersFormComponent.prototype.save = function () {
        var _this = this;
        var siteAdmin = (this.admin ? 1 : 0);
        var now = new Date();
        this.user = {
            displayName: this.displayName,
            email: this.email,
            siteAdmin: siteAdmin,
            id: this.id || this.displayName.replace(/\s+/g, '.'),
            created: now,
            lastAccess: now,
        };
        if (this.action === 'add') {
            this._usersService.create(this.user).subscribe(function () {
                _this.goBack();
            });
        }
        else {
            this._usersService.update(this.id, this.user).subscribe(function () {
                _this.goBack();
            });
        }
    };
    return UsersFormComponent;
}());
UsersFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-user-form',
        template: __webpack_require__(1035),
        styles: [__webpack_require__(838)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["d" /* TdMediaService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["d" /* TdMediaService */]) === "function" && _c || Object])
], UsersFormComponent);

var _a, _b, _c;
//# sourceMappingURL=form.component.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UsersComponent = (function () {
    function UsersComponent(_titleService, _router, _loadingService, _dialogService, _snackBarService, _usersService, media) {
        this._titleService = _titleService;
        this._router = _router;
        this._loadingService = _loadingService;
        this._dialogService = _dialogService;
        this._snackBarService = _snackBarService;
        this._usersService = _usersService;
        this.media = media;
    }
    UsersComponent.prototype.goBack = function (route) {
        this._router.navigate(['/']);
    };
    UsersComponent.prototype.ngAfterViewInit = function () {
        // broadcast to all listener observables when loading the page
        this.media.broadcast();
        this._titleService.setTitle('Covalent Users');
        this.loadUsers();
    };
    UsersComponent.prototype.filterUsers = function (displayName) {
        if (displayName === void 0) { displayName = ''; }
        this.filteredUsers = this.users.filter(function (user) {
            return user.displayName.toLowerCase().indexOf(displayName.toLowerCase()) > -1;
        });
    };
    UsersComponent.prototype.loadUsers = function () {
        var _this = this;
        this._loadingService.register('users.list');
        this._usersService.query().subscribe(function (users) {
            _this.users = users;
            _this.filteredUsers = users;
            _this._loadingService.resolve('users.list');
        }, function (error) {
            _this._usersService.staticQuery().subscribe(function (users) {
                _this.users = users;
                _this.filteredUsers = users;
                _this._loadingService.resolve('users.list');
            });
        });
    };
    UsersComponent.prototype.deleteUser = function (id) {
        var _this = this;
        this._dialogService
            .openConfirm({ message: 'Are you sure you want to delete this user?' })
            .afterClosed().subscribe(function (confirm) {
            if (confirm) {
                _this._loadingService.register('users.list');
                _this._usersService.delete(id).subscribe(function () {
                    _this.users = _this.users.filter(function (user) {
                        return user.id !== id;
                    });
                    _this.filteredUsers = _this.filteredUsers.filter(function (user) {
                        return user.id !== id;
                    });
                    _this._loadingService.resolve('users.list');
                    _this._snackBarService.open('User deleted', 'Ok');
                }, function (error) {
                    _this._dialogService.openAlert({ message: 'There was an error' });
                    _this._loadingService.resolve('users.list');
                });
            }
        });
    };
    return UsersComponent;
}());
UsersComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-users',
        template: __webpack_require__(1036),
        styles: [__webpack_require__(839)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_5__services__["b" /* UsersService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__covalent_core__["f" /* TdLoadingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__covalent_core__["f" /* TdLoadingService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__covalent_core__["g" /* TdDialogService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__covalent_core__["g" /* TdDialogService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_material__["M" /* MdSnackBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_material__["M" /* MdSnackBar */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services__["b" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services__["b" /* UsersService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4__covalent_core__["d" /* TdMediaService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__covalent_core__["d" /* TdMediaService */]) === "function" && _g || Object])
], UsersComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=users.component.js.map

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import 'rxjs/add/operator/catch';
var TrendsService = (function () {
    function TrendsService(http) {
        this.http = http;
    }
    // TODO Get trends from server API
    TrendsService.prototype.getAllTrends = function () {
        return this.http.get('/api/trends')
            .map(function (res) { return res.json(); });
        //.catch(this.handleError);
    };
    return TrendsService;
}());
TrendsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], TrendsService);

var _a;
//# sourceMappingURL=trends.service.js.map

/***/ }),

/***/ 373:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 373;


/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app___ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(439);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app___["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__(3);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent(_iconRegistry, _domSanitizer) {
        this._iconRegistry = _iconRegistry;
        this._domSanitizer = _domSanitizer;
        this._iconRegistry.addSvgIconInNamespace('assets', 'teradata', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/teradata.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'github', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/github.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'covalent', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/covalent.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'covalent-mark', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/covalent-mark.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'teradata-ux', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/teradata-ux.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'appcenter', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/appcenter.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'listener', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/listener.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'querygrid', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/querygrid.svg'));
        this._iconRegistry.addSvgIconInNamespace('assets', 'twitter', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/twitter_logo.svg'));
        this._domSanitizer.bypassSecurityTrustResourceUrl('assets/worlddata/world-110m.json');
        this._domSanitizer.bypassSecurityTrustResourceUrl('assets/worlddata/world-110m-withlakes.json');
        this._domSanitizer.bypassSecurityTrustResourceUrl('assets/worlddata/world-country-names.tsv');
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-app',
        template: __webpack_require__(1019),
        styles: [__webpack_require__(822)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_material__["N" /* MdIconRegistry */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_material__["N" /* MdIconRegistry */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["DomSanitizer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["DomSanitizer"]) === "function" && _b || Object])
], AppComponent);

var _a, _b;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_hammerjs__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__covalent_http__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__covalent_highlight__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__covalent_markdown__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__main_main_component__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__dashboard_dashboard_component__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__users_users_component__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__users_form_form_component__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__logs_logs_component__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__form_form_component__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__detail_detail_component__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__about_about_component__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__dashboard_product_dashboard_product_component__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__dashboard_product_overview_overview_component__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__dashboard_product_stats_stats_component__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__dashboard_product_features_features_component__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__dashboard_product_features_form_form_component__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__templates_templates_component__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__templates_dashboard_dashboard_component__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__templates_email_email_component__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__templates_editor_editor_component__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__app_routes__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__config_interceptors_request_interceptor__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__swimlane_ngx_charts__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_30__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__globe_globe_component__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__services_trends_service__ = __webpack_require__(260);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





//import { RouterModule } from '@angular/router';




























var httpInterceptorProviders = [
    __WEBPACK_IMPORTED_MODULE_29__config_interceptors_request_interceptor__["a" /* RequestInterceptor */],
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_11__main_main_component__["a" /* MainComponent */],
            __WEBPACK_IMPORTED_MODULE_12__dashboard_dashboard_component__["a" /* DashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_19__dashboard_product_dashboard_product_component__["a" /* DashboardProductComponent */],
            __WEBPACK_IMPORTED_MODULE_20__dashboard_product_overview_overview_component__["a" /* ProductOverviewComponent */],
            __WEBPACK_IMPORTED_MODULE_21__dashboard_product_stats_stats_component__["a" /* ProductStatsComponent */],
            __WEBPACK_IMPORTED_MODULE_22__dashboard_product_features_features_component__["a" /* ProductFeaturesComponent */],
            __WEBPACK_IMPORTED_MODULE_23__dashboard_product_features_form_form_component__["a" /* FeaturesFormComponent */],
            __WEBPACK_IMPORTED_MODULE_13__users_users_component__["a" /* UsersComponent */],
            __WEBPACK_IMPORTED_MODULE_14__users_form_form_component__["a" /* UsersFormComponent */],
            __WEBPACK_IMPORTED_MODULE_15__logs_logs_component__["a" /* LogsComponent */],
            __WEBPACK_IMPORTED_MODULE_16__form_form_component__["a" /* FormComponent */],
            __WEBPACK_IMPORTED_MODULE_17__detail_detail_component__["a" /* DetailComponent */],
            __WEBPACK_IMPORTED_MODULE_18__about_about_component__["a" /* AboutComponent */],
            __WEBPACK_IMPORTED_MODULE_24__templates_templates_component__["a" /* TemplatesComponent */],
            __WEBPACK_IMPORTED_MODULE_25__templates_dashboard_dashboard_component__["a" /* DashboardTemplateComponent */],
            __WEBPACK_IMPORTED_MODULE_26__templates_email_email_component__["a" /* EmailTemplateComponent */],
            __WEBPACK_IMPORTED_MODULE_27__templates_editor_editor_component__["a" /* EditorTemplateComponent */],
            __WEBPACK_IMPORTED_MODULE_31__globe_globe_component__["a" /* GlobeComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            //RouterModule.forRoot(ROUTES),
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["a" /* MaterialModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_6__covalent_core__["a" /* CovalentCoreModule */],
            __WEBPACK_IMPORTED_MODULE_7__covalent_http__["a" /* CovalentHttpModule */].forRoot({
                interceptors: [{
                        interceptor: __WEBPACK_IMPORTED_MODULE_29__config_interceptors_request_interceptor__["a" /* RequestInterceptor */], paths: ['**'],
                    }],
            }),
            __WEBPACK_IMPORTED_MODULE_8__covalent_highlight__["a" /* CovalentHighlightModule */],
            __WEBPACK_IMPORTED_MODULE_9__covalent_markdown__["a" /* CovalentMarkdownModule */],
            __WEBPACK_IMPORTED_MODULE_28__app_routes__["a" /* appRoutes */],
            __WEBPACK_IMPORTED_MODULE_30__swimlane_ngx_charts__["NgxChartsModule"],
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_28__app_routes__["b" /* appRoutingProviders */],
            httpInterceptorProviders,
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"],
            __WEBPACK_IMPORTED_MODULE_32__services_trends_service__["a" /* TrendsService */],
        ],
        entryComponents: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]],
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__main_main_component__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_product_dashboard_product_component__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_product_overview_overview_component__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_product_stats_stats_component__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dashboard_product_features_features_component__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__dashboard_product_features_form_form_component__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__users_users_component__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__users_form_form_component__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__logs_logs_component__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__detail_detail_component__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__about_about_component__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__form_form_component__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__templates_templates_component__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__templates_dashboard_dashboard_component__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__templates_email_email_component__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__templates_editor_editor_component__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__globe_globe_component__ = __webpack_require__(251);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return appRoutingProviders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return appRoutes; });


















var routes = [
    { path: 'about', component: __WEBPACK_IMPORTED_MODULE_11__about_about_component__["a" /* AboutComponent */] },
    { path: '', component: __WEBPACK_IMPORTED_MODULE_1__main_main_component__["a" /* MainComponent */], children: [{
                component: __WEBPACK_IMPORTED_MODULE_17__globe_globe_component__["a" /* GlobeComponent */],
                path: '',
            },
            { path: 'product', component: __WEBPACK_IMPORTED_MODULE_2__dashboard_product_dashboard_product_component__["a" /* DashboardProductComponent */], children: [
                    { path: '', component: __WEBPACK_IMPORTED_MODULE_3__dashboard_product_overview_overview_component__["a" /* ProductOverviewComponent */] },
                    { path: 'stats', component: __WEBPACK_IMPORTED_MODULE_4__dashboard_product_stats_stats_component__["a" /* ProductStatsComponent */] },
                    { path: 'features', children: [
                            { path: '', component: __WEBPACK_IMPORTED_MODULE_5__dashboard_product_features_features_component__["a" /* ProductFeaturesComponent */] },
                            { path: 'add', component: __WEBPACK_IMPORTED_MODULE_6__dashboard_product_features_form_form_component__["a" /* FeaturesFormComponent */] },
                            { path: ':id/delete', component: __WEBPACK_IMPORTED_MODULE_6__dashboard_product_features_form_form_component__["a" /* FeaturesFormComponent */] },
                            { path: ':id/edit', component: __WEBPACK_IMPORTED_MODULE_6__dashboard_product_features_form_form_component__["a" /* FeaturesFormComponent */] },
                        ] },
                ] },
            { path: 'item/:id', component: __WEBPACK_IMPORTED_MODULE_10__detail_detail_component__["a" /* DetailComponent */] },
            { path: 'logs', component: __WEBPACK_IMPORTED_MODULE_9__logs_logs_component__["a" /* LogsComponent */] },
            { path: 'form', component: __WEBPACK_IMPORTED_MODULE_12__form_form_component__["a" /* FormComponent */] },
            { path: 'users', children: [
                    { path: '', component: __WEBPACK_IMPORTED_MODULE_7__users_users_component__["a" /* UsersComponent */] },
                    { path: 'add', component: __WEBPACK_IMPORTED_MODULE_8__users_form_form_component__["a" /* UsersFormComponent */] },
                    { path: ':id/delete', component: __WEBPACK_IMPORTED_MODULE_8__users_form_form_component__["a" /* UsersFormComponent */] },
                    { path: ':id/edit', component: __WEBPACK_IMPORTED_MODULE_8__users_form_form_component__["a" /* UsersFormComponent */] },
                ] },
            { path: 'templates', children: [
                    { path: '', component: __WEBPACK_IMPORTED_MODULE_13__templates_templates_component__["a" /* TemplatesComponent */] },
                    { path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_14__templates_dashboard_dashboard_component__["a" /* DashboardTemplateComponent */] },
                    { path: 'email', component: __WEBPACK_IMPORTED_MODULE_15__templates_email_email_component__["a" /* EmailTemplateComponent */] },
                    { path: 'editor', component: __WEBPACK_IMPORTED_MODULE_16__templates_editor_editor_component__["a" /* EditorTemplateComponent */] },
                ] },
        ] },
];
var appRoutingProviders = [];
var appRoutes = __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* RouterModule */].forRoot(routes, { useHash: true });
//# sourceMappingURL=app.routes.js.map

/***/ }),

/***/ 431:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return single; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return multi; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return multi2; });
var single = [
    {
        'name': 'Databases',
        'value': 382941,
    },
    {
        'name': 'Containers',
        'value': 152294,
    },
    {
        'name': 'Streams',
        'value': 283000,
    },
    {
        'name': 'Queries',
        'value': 828921,
    },
];
var multi = [
    {
        'name': 'Databases',
        'series': [
            {
                'value': 75,
                'name': '2017-02-07T07:57:57Z'
            }, {
                'value': 93,
                'name': '2017-02-10T14:04:37Z'
            }, {
                'value': 88,
                'name': '2017-02-26T03:56:27Z'
            }, {
                'value': 91,
                'name': '2017-02-13T04:00:58Z'
            }, {
                'value': 99,
                'name': '2017-02-11T08:15:08Z'
            }, {
                'value': 73,
                'name': '2017-02-08T08:32:17Z'
            }, {
                'value': 94,
                'name': '2017-02-27T18:02:21Z'
            }, {
                'value': 80,
                'name': '2017-02-25T03:18:22Z'
            }, {
                'value': 95,
                'name': '2017-02-16T08:15:41Z'
            }, {
                'value': 74,
                'name': '2017-02-27T13:45:41Z'
            }, {
                'value': 89,
                'name': '2017-02-12T22:35:11Z'
            }, {
                'value': 99,
                'name': '2017-02-12T19:23:20Z'
            }, {
                'value': 80,
                'name': '2017-02-26T04:22:37Z'
            }, {
                'value': 86,
                'name': '2017-02-02T13:40:04Z'
            }, {
                'value': 90,
                'name': '2017-02-09T08:30:16Z'
            }, {
                'value': 77,
                'name': '2017-02-26T16:35:01Z'
            }, {
                'value': 83,
                'name': '2017-02-20T02:11:26Z'
            }, {
                'value': 74,
                'name': '2017-02-23T13:43:18Z'
            }, {
                'value': 79,
                'name': '2017-02-24T14:58:13Z'
            }, {
                'value': 75,
                'name': '2017-02-03T15:43:57Z'
            }, {
                'value': 76,
                'name': '2017-02-10T12:32:17Z'
            }, {
                'value': 72,
                'name': '2017-02-10T14:27:48Z'
            }, {
                'value': 93,
                'name': '2017-02-22T16:50:25Z'
            }, {
                'value': 99,
                'name': '2017-02-21T08:55:32Z'
            }, {
                'value': 93,
                'name': '2017-02-06T03:51:59Z'
            }, {
                'value': 99,
                'name': '2017-02-13T19:04:17Z'
            }, {
                'value': 70,
                'name': '2017-02-21T06:46:02Z'
            }, {
                'value': 90,
                'name': '2017-02-15T01:05:22Z'
            },
        ],
    },
    {
        'name': 'Containers',
        'series': [
            {
                'value': 94,
                'name': '2017-02-24T11:37:55Z'
            }, {
                'value': 72,
                'name': '2017-02-13T01:39:20Z'
            }, {
                'value': 78,
                'name': '2017-02-03T07:42:11Z'
            }, {
                'value': 69,
                'name': '2017-02-27T08:23:37Z'
            }, {
                'value': 85,
                'name': '2017-02-23T10:00:51Z'
            }, {
                'value': 69,
                'name': '2017-02-07T03:53:12Z'
            }, {
                'value': 86,
                'name': '2017-02-01T09:59:36Z'
            }, {
                'value': 79,
                'name': '2017-02-26T05:11:41Z'
            }, {
                'value': 76,
                'name': '2017-02-06T01:55:49Z'
            }, {
                'value': 69,
                'name': '2017-02-11T10:00:58Z'
            }, {
                'value': 93,
                'name': '2017-02-06T13:47:53Z'
            }, {
                'value': 73,
                'name': '2017-02-26T22:27:30Z'
            }, {
                'value': 92,
                'name': '2017-02-09T10:37:05Z'
            }, {
                'value': 91,
                'name': '2017-02-06T01:43:20Z'
            }, {
                'value': 82,
                'name': '2017-02-11T08:36:36Z'
            }, {
                'value': 83,
                'name': '2017-02-26T10:44:06Z'
            }, {
                'value': 98,
                'name': '2017-02-13T05:55:33Z'
            }, {
                'value': 98,
                'name': '2017-02-20T17:08:54Z'
            }, {
                'value': 71,
                'name': '2017-02-08T14:55:44Z'
            }, {
                'value': 76,
                'name': '2017-02-19T15:01:52Z'
            }, {
                'value': 71,
                'name': '2017-02-25T09:01:02Z'
            }, {
                'value': 85,
                'name': '2017-02-09T12:39:41Z'
            }, {
                'value': 99,
                'name': '2017-02-05T00:04:20Z'
            }, {
                'value': 74,
                'name': '2017-02-13T09:06:47Z'
            }, {
                'value': 94,
                'name': '2017-02-18T22:26:56Z'
            }, {
                'value': 95,
                'name': '2017-02-20T19:35:02Z'
            }, {
                'value': 79,
                'name': '2017-02-26T13:47:28Z'
            }, {
                'value': 82,
                'name': '2017-02-03T22:25:53Z'
            },
        ],
    },
];
var multi2 = [
    {
        'name': 'Queries',
        'series': [
            {
                'value': 86,
                'name': '2017-02-18T05:42:03Z'
            }, {
                'value': 78,
                'name': '2017-02-19T10:28:34Z'
            }, {
                'value': 95,
                'name': '2017-02-20T18:04:56Z'
            }, {
                'value': 99,
                'name': '2017-02-09T03:46:09Z'
            }, {
                'value': 96,
                'name': '2017-02-23T21:38:14Z'
            }, {
                'value': 76,
                'name': '2017-02-25T04:02:58Z'
            }, {
                'value': 93,
                'name': '2017-02-16T04:11:43Z'
            }, {
                'value': 88,
                'name': '2017-02-25T21:16:49Z'
            }, {
                'value': 74,
                'name': '2017-02-05T02:57:50Z'
            }, {
                'value': 92,
                'name': '2017-02-11T14:28:06Z'
            }, {
                'value': 86,
                'name': '2017-02-22T11:50:09Z'
            }, {
                'value': 92,
                'name': '2017-02-27T05:26:53Z'
            }, {
                'value': 71,
                'name': '2017-02-10T11:21:41Z'
            }, {
                'value': 94,
                'name': '2017-02-01T04:11:49Z'
            }, {
                'value': 69,
                'name': '2017-02-23T19:01:52Z'
            }, {
                'value': 69,
                'name': '2017-02-12T08:13:44Z'
            }, {
                'value': 87,
                'name': '2017-02-18T12:18:38Z'
            }, {
                'value': 99,
                'name': '2017-02-13T04:00:55Z'
            }, {
                'value': 94,
                'name': '2017-02-06T02:19:15Z'
            }, {
                'value': 75,
                'name': '2017-02-11T14:13:24Z'
            }, {
                'value': 93,
                'name': '2017-02-21T05:32:06Z'
            }, {
                'value': 76,
                'name': '2017-02-08T14:35:45Z'
            }, {
                'value': 85,
                'name': '2017-02-15T00:13:20Z'
            }, {
                'value': 80,
                'name': '2017-02-26T23:03:11Z'
            }, {
                'value': 85,
                'name': '2017-02-08T05:48:51Z'
            }, {
                'value': 81,
                'name': '2017-02-20T07:13:16Z'
            }, {
                'value': 87,
                'name': '2017-02-21T06:23:07Z'
            }, {
                'value': 81,
                'name': '2017-02-27T22:27:23Z'
            },
        ],
    },
    {
        'name': 'Databases',
        'series': [
            {
                'value': 68,
                'name': '2017-02-16T06:36:47Z'
            }, {
                'value': 76,
                'name': '2017-02-21T05:55:24Z'
            }, {
                'value': 95,
                'name': '2017-02-03T16:38:57Z'
            }, {
                'value': 86,
                'name': '2017-02-15T19:30:36Z'
            }, {
                'value': 76,
                'name': '2017-02-10T23:27:01Z'
            }, {
                'value': 74,
                'name': '2017-02-10T05:14:01Z'
            }, {
                'value': 83,
                'name': '2017-02-14T16:54:56Z'
            }, {
                'value': 88,
                'name': '2017-02-07T14:27:31Z'
            }, {
                'value': 74,
                'name': '2017-02-14T21:52:15Z'
            }, {
                'value': 90,
                'name': '2017-02-17T16:26:38Z'
            }, {
                'value': 95,
                'name': '2017-02-26T08:34:58Z'
            }, {
                'value': 97,
                'name': '2017-02-14T07:36:17Z'
            }, {
                'value': 99,
                'name': '2017-02-05T20:23:59Z'
            }, {
                'value': 89,
                'name': '2017-02-27T11:55:41Z'
            }, {
                'value': 94,
                'name': '2017-02-16T22:40:05Z'
            }, {
                'value': 90,
                'name': '2017-02-08T08:18:50Z'
            }, {
                'value': 68,
                'name': '2017-02-25T20:55:32Z'
            }, {
                'value': 90,
                'name': '2017-02-09T18:19:08Z'
            }, {
                'value': 98,
                'name': '2017-02-07T03:58:38Z'
            }, {
                'value': 68,
                'name': '2017-02-22T19:20:24Z'
            }, {
                'value': 86,
                'name': '2017-02-06T22:22:22Z'
            }, {
                'value': 83,
                'name': '2017-02-17T20:52:51Z'
            }, {
                'value': 96,
                'name': '2017-02-21T21:48:03Z'
            }, {
                'value': 97,
                'name': '2017-02-22T17:15:39Z'
            }, {
                'value': 75,
                'name': '2017-02-20T06:10:57Z'
            }, {
                'value': 73,
                'name': '2017-02-22T18:21:58Z'
            }, {
                'value': 92,
                'name': '2017-02-22T15:02:07Z'
            }, {
                'value': 92,
                'name': '2017-02-27T10:30:03Z'
            },
        ],
    },
];
//# sourceMappingURL=data.js.map

/***/ }),

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return single; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return multi; });
var single = [
    {
        'name': 'Databases',
        'value': 382941,
    },
    {
        'name': 'Containers',
        'value': 152294,
    },
    {
        'name': 'Streams',
        'value': 283000,
    },
    {
        'name': 'Queries',
        'value': 828921,
    },
];
var multi = [
    {
        'name': 'Databases',
        'series': [
            {
                'value': 2469,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 3619,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 3885,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 4289,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 3309,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Containers',
        'series': [
            {
                'value': 2452,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 4938,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 4110,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 3828,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5772,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Queries',
        'series': [
            {
                'value': 4022,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 2345,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 5148,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 6868,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5415,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Databases',
        'series': [
            {
                'value': 6194,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 6585,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 6857,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 2545,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5986,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
];
//# sourceMappingURL=data.js.map

/***/ }),

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__covalent_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__data__ = __webpack_require__(434);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DashboardComponent = (function () {
    function DashboardComponent(_titleService, _itemsService, _usersService, _alertsService, _productsService, _loadingService) {
        this._titleService = _titleService;
        this._itemsService = _itemsService;
        this._usersService = _usersService;
        this._alertsService = _alertsService;
        this._productsService = _productsService;
        this._loadingService = _loadingService;
        this.view = [700, 400];
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = false;
        this.showXAxisLabel = true;
        this.xAxisLabel = '';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Sales';
        this.colorScheme = {
            domain: ['#1565C0', '#2196F3', '#81D4FA', '#FF9800', '#EF6C00'],
        };
        // line, area
        this.autoScale = true;
        // Chart
        this.multi = __WEBPACK_IMPORTED_MODULE_4__data__["a" /* multi */].map(function (group) {
            group.series = group.series.map(function (dataItem) {
                dataItem.name = new Date(dataItem.name);
                return dataItem;
            });
            return group;
        });
    }
    DashboardComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this._titleService.setTitle('Covalent Quickstart');
        this._loadingService.register('items.load');
        this._itemsService.query().subscribe(function (items) {
            _this.items = items;
            setTimeout(function () {
                _this._loadingService.resolve('items.load');
            }, 750);
        }, function (error) {
            _this._itemsService.staticQuery().subscribe(function (items) {
                _this.items = items;
                setTimeout(function () {
                    _this._loadingService.resolve('items.load');
                }, 750);
            });
        });
        this._loadingService.register('alerts.load');
        this._alertsService.query().subscribe(function (alerts) {
            _this.alerts = alerts;
            setTimeout(function () {
                _this._loadingService.resolve('alerts.load');
            }, 750);
        });
        this._loadingService.register('products.load');
        this._productsService.query().subscribe(function (products) {
            _this.products = products;
            setTimeout(function () {
                _this._loadingService.resolve('products.load');
            }, 750);
        });
        this._loadingService.register('favorites.load');
        this._productsService.query().subscribe(function (products) {
            _this.products = products;
            setTimeout(function () {
                _this._loadingService.resolve('favorites.load');
            }, 750);
        });
        this._loadingService.register('users.load');
        this._usersService.query().subscribe(function (users) {
            _this.users = users;
            setTimeout(function () {
                _this._loadingService.resolve('users.load');
            }, 750);
        }, function (error) {
            _this._usersService.staticQuery().subscribe(function (users) {
                _this.users = users;
                setTimeout(function () {
                    _this._loadingService.resolve('users.load');
                }, 750);
            });
        });
    };
    // ngx transform using covalent digits pipe
    DashboardComponent.prototype.axisDigits = function (val) {
        return new __WEBPACK_IMPORTED_MODULE_2__covalent_core__["h" /* TdDigitsPipe */]().transform(val);
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qs-dashboard',
        template: __webpack_require__(1025),
        styles: [__webpack_require__(828)],
        viewProviders: [__WEBPACK_IMPORTED_MODULE_3__services__["a" /* ItemsService */], __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */], __WEBPACK_IMPORTED_MODULE_3__services__["c" /* ProductsService */], __WEBPACK_IMPORTED_MODULE_3__services__["e" /* AlertsService */]],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services__["a" /* ItemsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["a" /* ItemsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["b" /* UsersService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services__["e" /* AlertsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["e" /* AlertsService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__services__["c" /* ProductsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["c" /* ProductsService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__covalent_core__["f" /* TdLoadingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__covalent_core__["f" /* TdLoadingService */]) === "function" && _f || Object])
], DashboardComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ 434:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export single */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return multi; });
var single = [
    {
        'name': 'Germany',
        'value': 8940000,
    },
    {
        'name': 'USA',
        'value': 5000000,
    },
    {
        'name': 'France',
        'value': 7200000,
    },
];
var multi = [
    {
        'name': 'Container Apps',
        'series': [
            {
                'value': 2469,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 3619,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 3885,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 4289,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 3309,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Alerting Dashboard',
        'series': [
            {
                'value': 2452,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 4938,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 4110,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 3828,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5772,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Streaming Data',
        'series': [
            {
                'value': 4022,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 2345,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 5148,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 6868,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5415,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Database Queries',
        'series': [
            {
                'value': 6194,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 6585,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 6857,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 2545,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5986,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'NoSQL Database',
        'series': [
            {
                'value': 4260,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 4810,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 5087,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 5941,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 6427,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
];
//# sourceMappingURL=data.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_module__ = __webpack_require__(429);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__app_module__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return single; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return multi; });
var single = [
    {
        'name': 'Germany',
        'value': 8940000,
    },
    {
        'name': 'USA',
        'value': 5000000,
    },
    {
        'name': 'France',
        'value': 7200000,
    },
];
var multi = [
    {
        'name': 'Containers',
        'series': [
            {
                'value': 2469,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 3619,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 3885,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 4289,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 3309,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Alerts',
        'series': [
            {
                'value': 2452,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 4938,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 4110,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 3828,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5772,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Streams',
        'series': [
            {
                'value': 4022,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 2345,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 5148,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 6868,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5415,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Databases',
        'series': [
            {
                'value': 6194,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 6585,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 6857,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 2545,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5986,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'NoSQL',
        'series': [
            {
                'value': 4260,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 4810,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 5087,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 5941,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 6427,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
];
//# sourceMappingURL=data.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export single */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return multi; });
var single = [
    {
        'name': 'Germany',
        'value': 8940000,
    },
    {
        'name': 'USA',
        'value': 5000000,
    },
    {
        'name': 'France',
        'value': 7200000,
    },
];
var multi = [
    {
        'name': 'Containers',
        'series': [
            {
                'value': 2469,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 3619,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 3885,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 4289,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 3309,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Alerts',
        'series': [
            {
                'value': 2452,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 4938,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 4110,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 3828,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5772,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Streams',
        'series': [
            {
                'value': 4022,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 2345,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 5148,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 6868,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5415,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'Databases',
        'series': [
            {
                'value': 6194,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 6585,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 6857,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 2545,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 5986,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
    {
        'name': 'NoSQL',
        'series': [
            {
                'value': 4260,
                'name': '2016-09-15T19:25:07.773Z',
            },
            {
                'value': 4810,
                'name': '2016-09-17T17:16:53.279Z',
            },
            {
                'value': 5087,
                'name': '2016-09-15T10:34:32.344Z',
            },
            {
                'value': 5941,
                'name': '2016-09-19T14:33:45.710Z',
            },
            {
                'value': 6427,
                'name': '2016-09-12T18:48:58.925Z',
            },
        ],
    },
];
//# sourceMappingURL=data.js.map

/***/ }),

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestInterceptor; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var RequestInterceptor = (function () {
    function RequestInterceptor() {
    }
    RequestInterceptor.prototype.onRequest = function (requestOptions) {
        // you add headers or do something before a request here.
        return requestOptions;
    };
    return RequestInterceptor;
}());
RequestInterceptor = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], RequestInterceptor);

//# sourceMappingURL=request.interceptor.js.map

/***/ }),

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 440:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__covalent_http__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertsService = (function () {
    function AlertsService(_http) {
        this._http = _http;
    }
    AlertsService.prototype.query = function () {
        return this._http.get('data/alerts.json')
            .map(function (res) {
            return res.json();
        });
    };
    AlertsService.prototype.get = function (id) {
        return this._http.get('data/alerts.json')
            .map(function (res) {
            var item;
            res.json().forEach(function (s) {
                if (s.item_id === id) {
                    item = s;
                }
            });
            return item;
        });
    };
    return AlertsService;
}());
AlertsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */]) === "function" && _a || Object])
], AlertsService);

var _a;
//# sourceMappingURL=alerts.service.js.map

/***/ }),

/***/ 441:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__covalent_http__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_api_config__ = __webpack_require__(128);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeaturesService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeaturesService = (function (_super) {
    __extends(FeaturesService, _super);
    function FeaturesService(_http) {
        var _this = _super.call(this, _http, {
            baseUrl: __WEBPACK_IMPORTED_MODULE_2__config_api_config__["a" /* MOCK_API */],
            path: '/features',
        }) || this;
        _this._http = _http;
        return _this;
    }
    FeaturesService.prototype.staticQuery = function () {
        return this._http.get('data/features.json')
            .map(function (res) {
            return res.json();
        });
    };
    return FeaturesService;
}(__WEBPACK_IMPORTED_MODULE_1__covalent_http__["b" /* RESTService */]));
FeaturesService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */]) === "function" && _a || Object])
], FeaturesService);

var _a;
//# sourceMappingURL=features.service.js.map

/***/ }),

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__covalent_http__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_api_config__ = __webpack_require__(128);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemsService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ItemsService = (function (_super) {
    __extends(ItemsService, _super);
    function ItemsService(_http) {
        var _this = _super.call(this, _http, {
            baseUrl: __WEBPACK_IMPORTED_MODULE_2__config_api_config__["a" /* MOCK_API */],
            path: '/items',
        }) || this;
        _this._http = _http;
        return _this;
    }
    ItemsService.prototype.staticQuery = function () {
        return this._http.get('data/items.json')
            .map(function (res) {
            return res.json();
        });
    };
    ItemsService.prototype.staticGet = function (id) {
        return this._http.get('data/items.json')
            .map(function (res) {
            var item;
            res.json().forEach(function (s) {
                if (s.item_id === id) {
                    item = s;
                }
            });
            return item;
        });
    };
    return ItemsService;
}(__WEBPACK_IMPORTED_MODULE_1__covalent_http__["b" /* RESTService */]));
ItemsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */]) === "function" && _a || Object])
], ItemsService);

var _a;
//# sourceMappingURL=items.service.js.map

/***/ }),

/***/ 443:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__covalent_http__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductsService = (function () {
    function ProductsService(_http) {
        this._http = _http;
    }
    ProductsService.prototype.query = function () {
        return this._http.get('data/products.json')
            .map(function (res) {
            return res.json();
        });
    };
    ProductsService.prototype.get = function (id) {
        return this._http.get('data/products.json')
            .map(function (res) {
            var item;
            res.json().forEach(function (s) {
                if (s.item_id === id) {
                    item = s;
                }
            });
            return item;
        });
    };
    return ProductsService;
}());
ProductsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */]) === "function" && _a || Object])
], ProductsService);

var _a;
//# sourceMappingURL=products.service.js.map

/***/ }),

/***/ 444:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__covalent_http__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_api_config__ = __webpack_require__(128);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsersService = (function (_super) {
    __extends(UsersService, _super);
    function UsersService(_http) {
        var _this = _super.call(this, _http, {
            baseUrl: __WEBPACK_IMPORTED_MODULE_2__config_api_config__["a" /* MOCK_API */],
            path: '/users',
        }) || this;
        _this._http = _http;
        return _this;
    }
    UsersService.prototype.staticQuery = function () {
        return this._http.get('data/users.json')
            .map(function (res) {
            return res.json();
        });
    };
    return UsersService;
}(__WEBPACK_IMPORTED_MODULE_1__covalent_http__["b" /* RESTService */]));
UsersService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__covalent_http__["c" /* HttpInterceptorService */]) === "function" && _a || Object])
], UsersService);

var _a;
//# sourceMappingURL=users.service.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__users_service__ = __webpack_require__(444);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__users_service__["a"]; });
/* unused harmony reexport IUser */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__items_service__ = __webpack_require__(442);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__items_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__products_service__ = __webpack_require__(443);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__products_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alerts_service__ = __webpack_require__(440);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_3__alerts_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__features_service__ = __webpack_require__(441);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_4__features_service__["a"]; });
/* unused harmony reexport IFeature */





//# sourceMappingURL=index.js.map

/***/ }),

/***/ 821:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ":host /deep/ md-divider {\n  display: block;\n  border-top-style: solid;\n  border-top-width: 1px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 822:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "/* :host /deep/ lets shadowdom style child elements */\n:host /deep/ {\n  /**\n    * CSS Overrides for bug fixes\n    */\n  /**\n    * END CSS Overrides for bug fixes\n    */\n  /* Manage list custom styles */ }\n  :host /deep/ .md-sort-item /deep/ .md-list-item-content {\n    padding: 0; }\n  :host /deep/ .md-sort-icon {\n    font-size: 15px;\n    margin-right: 10px; }\n  :host /deep/ .md-sort-header {\n    padding: 10px; }\n    :host /deep/ .md-sort-header:hover {\n      background-color: #EEEEEE;\n      cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 823:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 824:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 825:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 826:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ":host /deep/ .tick text {\n  font-size: 24px; }\n\n.chart-height {\n  height: 200px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 827:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ".chart-height {\n  height: 250px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 828:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ".md-icon-ux {\n  width: 100px; }\n\n.icon {\n  font-size: 48px; }\n\n.chart-height {\n  height: 300px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 829:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 830:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 831:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ".md-icon-ux {\n  width: 100px; }\n\n.icon {\n  font-size: 48px; }\n\n.title {\n  position: absolute;\n  top: 500px;\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  text-align: center;\n  width: 960px; }\n\n.realtitle {\n  position: absolute;\n  top: -5px;\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  text-align: center;\n  width: 960px;\n  font-size: 17px;\n  color: #4099FF;\n  font-weight: bold; }\n\n.search {\n  position: absolute;\n  top: -5px;\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  text-align: center;\n  width: 960px;\n  font-size: 17px;\n  font-weight: bold; }\n\n.inner-title {\n  font-size: 18px;\n  color: #1DA1F3; }\n\n.twitter-logo-a {\n  top: 40px;\n  left: 0px; }\n\n.twitter-logo-b {\n  position: absolute;\n  top: -17px;\n  left: 635px; }\n\n.tweet {\n  position: absolute;\n  left: 240px;\n  width: 480px;\n  text-align: center; }\n\n.github-mark {\n  position: absolute;\n  left: 45px;\n  top: 25px; }\n\n.github {\n  position: absolute;\n  left: 25px;\n  top: 100px; }\n\na {\n  text-decoration: none;\n  font-family: \"Helvetica Neue\", Roboto, \"Segoe UI\", Calibri, sans-serif; }\n\nblockquote.twitter-tweet {\n  display: inline-block;\n  font-family: \"Helvetica Neue\", Roboto, \"Segoe UI\", Calibri, sans-serif;\n  font-size: 14px;\n  font-weight: bold;\n  line-height: 16px;\n  border-color: #eee #ddd #bbb;\n  border-radius: 5px;\n  border-style: solid;\n  border-width: 1px;\n  box-shadow: 0 1px 3px rgba(255, 255, 255, 0.82);\n  background-color: rgba(255, 255, 255, 0.82);\n  margin: 10px 5px;\n  padding: 0 16px 16px 16px;\n  max-width: 468px; }\n\nblockquote.twitter-tweet p {\n  font-size: 16px;\n  font-weight: normal;\n  line-height: 20px; }\n\nblockquote.twitter-tweet a {\n  color: inherit;\n  font-weight: normal;\n  font-family: \"Helvetica Neue\", Roboto, \"Segoe UI\", Calibri, sans-serif;\n  text-decoration: none;\n  outline: 0 none; }\n\nblockquote.twitter-tweet a:hover,\nblockquote.twitter-tweet a:focus {\n  text-decoration: underline;\n  color: #1DA1F3; }\n\n.twitter-user {\n  color: #1DA1F3; }\n\n#pauseButton {\n  position: absolute;\n  left: 900px; }\n\n#playButton {\n  position: absolute;\n  left: 1000px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 832:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 833:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 834:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ":host /deep/ text {\n  fill: #a8b2c7; }\n\n:host /deep/ .gridline-path {\n  stroke: #2f3646; }\n\n:host /deep/ .gridline-path-vertical {\n  display: none; }\n\n:host /deep/ .number-card p {\n  color: #f0f1f6; }\n\n:host /deep/ .gauge .background-arc path {\n  fill: #2f3646; }\n\n:host /deep/ .gauge .gauge-tick path {\n  stroke: #a8b2c7; }\n\n:host /deep/ .gauge .gauge-tick text {\n  fill: #a8b2c7; }\n\n:host /deep/ .linear-gauge .background-bar path {\n  fill: #2f3646; }\n\n:host /deep/ .linear-gauge .units {\n  fill: #72809b; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 835:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ":host /deep/ md-sidenav .cdk-focus-trap-content > * {\n  height: 100%; }\n\n:host /deep/ text {\n  fill: #a8b2c7; }\n\n:host /deep/ .gridline-path {\n  stroke: #2f3646; }\n\n:host /deep/ .gridline-path-vertical {\n  display: none; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 836:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, ":host /deep/ text {\n  fill: #a8b2c7; }\n\n:host /deep/ .gridline-path {\n  stroke: #2f3646; }\n\n:host /deep/ .gridline-path-vertical {\n  display: none; }\n\n:host /deep/ .number-card p {\n  color: #f0f1f6; }\n\n:host /deep/ .gauge .background-arc path {\n  fill: #2f3646; }\n\n:host /deep/ .gauge .gauge-tick path {\n  stroke: #a8b2c7; }\n\n:host /deep/ .gauge .gauge-tick text {\n  fill: #a8b2c7; }\n\n:host /deep/ .linear-gauge .background-bar path {\n  fill: #2f3646; }\n\n:host /deep/ .linear-gauge .units {\n  fill: #72809b; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 837:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 838:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 839:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ })

},[1094]);
//# sourceMappingURL=main.bundle.js.map