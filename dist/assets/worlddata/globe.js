// grab globe dimensions by finding height of browser window on page load
var width = window.innerHeight,
    height = window.innerHeight;

var graticule = d3.geo.graticule();

    var backGrid = graticule();
    var grid = graticule();

    var globe = {type: "Sphere"};

    var projection = d3.geo.orthographic()
        .scale(height / 2.1)
        .translate([width / 2, height / 2])
        .precision(0.6);

    var canvas = d3.select(".globebody").append("canvas")
        .attr("width", width)
        .attr("height", height);

    var c = canvas.node().getContext("2d");

    var path = d3.geo.path()
        .projection(projection)
        .context(c);

var title = d3.select(".title");
var currentTrend = d3.select(".current-trend");
queue()
    .defer(d3.json, "assets/worlddata/world-110m.json")
    .defer(d3.tsv, "assets/worlddata/world-country-names.tsv")
    .await(ready);

    // function to draw animated pings..not working at the moment
    function drawPing(lat, lng, pingColor, pingAngle, ttl) {
      //var now = new Date();
      var alpha = 1 - (ttl);
      var color = d3.rgb(pingColor);
      color = "rgba(" + color.r + "," + color.g + "," + color.b + "," + alpha + ")";
      c.strokeStyle = color;
      var circle = d3.geo.circle().origin([lat, lng])
        .angle(ttl * pingAngle)();
      c.beginPath();
      path.context(c)(circle);
      c.stroke();
    };

    // Open a socket to website.
    // function openSock(){
    //   return io({ "force new connection" : true });
    // }
    var socket = io({ "force new connection" : true });

    //var pausePlayButtonLabel = "Play"
    // Enter a new trend
    function newTerm(){
        //socket.close();
        substring = "";
        substring = document.getElementById("inputSearch").value;
        //socket.open();
        //socket.emit('searchTerm', {inputTerm: input});
        //console.log('Hey');
      }
    //When trend is checked on client side, execute funciton to add that trend to an array of trends that tweets are shown about
    function getTrendName(trend){
      var htmlString = trend.toString().trim();

      //document.getElementById("testStuff").innerHTML = htmlString;

      if(queries.indexOf(htmlString) > -1) {
        queries.splice(queries.indexOf(htmlString), 1);
        console.log(queries);
      } else {
        queries.push(htmlString);
        console.log(queries);
      }
    }
    // Pause stream function
    function stopConnection(){
      socket.close();
    }
    // Play stream function
    function openConnectionAgain(){
      socket.open();
    }
    // On page load, display tweets about Trump
    var queries = ["Trump",];

function ready(error, world, names) {
    if (error) throw error;
    var globe = {type: "Sphere"},
        land = topojson.feature(world, world.objects.land),
         countries = topojson.feature(world, world.objects.countries).features,
         borders = topojson.mesh(world, world.objects.countries, function(a, b) { return a !== b; });
     countries = countries.filter(function(d) {
       return names.some(function(n) {
         if (d.id == n.id) return d.name = n.name;
       });
     }).sort(function(a, b) {
       return a.name.localeCompare(b.name);
   });



  // Function to render embedded tweet
  // TODO - make this look better.
  function makeTweet(tweet) {
    return [
      '<blockquote class="twitter-tweet">',
      '<h1 class="inner-title">', '<img class="twitter-logo-a" src="assets/icons/twitter-512.gif" width="40" height="40">', '<b>', '&nbsp;', tweet.placeName, '</b>', '</h1>',
      '<a href="https://twitter.com/', tweet.user, '/status/', tweet.id, '" target="_blank">',
      tweet.text, '<br />','<br />', '</a>',
        '<a href="https://twitter.com/', tweet.user, '" target="_blank">', '<b>','&mdash; ', tweet.name, ' (', '@', tweet.user,')','</b>', '</a>',
       '</blockquote>',
    ].join('');
  }

  function makeCurrentTrend(trend) {
    return [
      '<b>',
      '"',
      trend,
      '"',
      '</b>',
    ].join('');
  }


  //tweet.text.includes(substring)==true || tweet.text.includes(otherSubstring)==true
  socket.on( 'tweet', function(tweet) {
    for(i = 0; i < queries.length; i++) {

      if(tweet.text.toLowerCase().indexOf(queries[i].toLowerCase()) >= 0) {
        console.log('tweet');
        d3.transition()
            .duration(1250)
            .each("start", function() {
           title.html(makeTweet(tweet));
           currentTrend.html(makeCurrentTrend(queries[i]));
         })
         .tween("rotate", function() {
           var p = tweet.latLong,
               r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
           return function(t) {

             // Rotate globe so that immediate point is centered in the view.
             projection.rotate(r(t));
             // Erase canvas.
             c.clearRect(0, 0, width, height);
             // Draw a circle at canvas-coordinates of latlong.
             projection.rotate(r(t)).clipAngle(180);
              c.fillStyle = "#a3e9db", c.beginPath(), path(land), c.fill();
              // c.fillStyle = "#f00", c.beginPath(), path(countries[i]), c.fill();
              c.strokeStyle = "#fff", c.lineWidth = .5, c.beginPath(), path(borders), c.stroke();
              c.strokeStyle = "#000", c.lineWidth = 1, c.beginPath(), path(globe), c.stroke();
              c.strokeStyle = "rgba(0, 0, 0, 0.05)", c.lineWidth = .5, c.beginPath(), path(backGrid), c.stroke();

              projection.rotate(r(t)).clipAngle(90);
              c.fillStyle = "#338373", c.beginPath(), path(land), c.fill();
              var center = projection(p);
              c.strokeStyle = "#1DA1F3", c.fillStyle = "#fff", c.beginPath(), c.arc(center[0], center[1], 8, 0, 2 * Math.PI, false), c.lineWidth = 5, c.fill(), c.stroke();
              c.strokeStyle = "#fff", c.lineWidth = .5, c.beginPath(), path(borders), c.stroke();
              c.strokeStyle = "rgba(0, 0, 0, 0.1)", c.lineWidth = 1, c.beginPath(), path(globe), c.stroke();
              c.strokeStyle = "rgba(0, 0, 0, 0.1)", c.lineWidth = .5, c.beginPath(), path(grid), c.stroke();

            //#1b7be9
            //#328df5
            //rgba(4, 148, 185, 1)
            //rgba(57, 162, 190, 0.94)
            //#5fc84e
            };
          })
          .transition()
            .each("end", transition);
          break; // break out of the for loop if query matches
      }

    }
  });

}

d3.select(self.frameElement).style("height", height + "px");
