const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const dbInfo = require('../../config/db');


// MongoClient.connect(dbInfo.url, (err, database) => {
//   if (err) {return console.log(err);}
//
//   //require('./server/routes/api')(app, database);
//   database.collection('trends').remove( {} );
//   database.collection('trends').insert(usa_trends, (err, result) => {
//     if (err) {
//       console.log('An error has occurred. Your data submission could not be completed.');
//     } else {
//       console.log('Data submission success!');
//     }
//   });
// })
//  Module as a function of Express instance and database in server.js.

  router.get('/', (req, res) => {
    res.send('api works');
  });

  /*  GET request that retrieves all daily trends from database and sends them
   *  to the client as an array of strings.
   */
  router.get('/trends', (req, res) => {

    return new Promise((resolve, reject) => {

      MongoClient.connect(dbInfo.url, (err, database) => {
        if (err) {
          reject(err);
        } else {
          resolve(database);
        }
      })
    })

    .then((database) => {
      /*  Return a promise that resolves when all items from the 'trends' collection
       *  are retrieved and converted to an array
       */
        return new Promise((resolve, reject) => {

          database.collection('trends').find({}).toArray((err, items) => {
            if (err) {
              reject(err);
            } else {
              console.log(items);
              resolve(items);
            }
          });
        })

        /*  Then, a function is called that sorts the trends array
         *  by trend.tweet_volume...
         */
        .then((items) => {
          items.sort((a,b) => {
            return b.tweet_volume - a.tweet_volume;
          });

          /*  And creates a new array called trendNamesList with only
           *  the trend names.
           */
          let trendNamesList = items.map((item) => {
            return item.name;
          });

          /*  I hardcoded 'Trump' to be inserted at trendNamesList[3] because
           *  he's always trending. Ha ha.
           */
          //trendNamesList.unshift("Trump");

          // Log the result before sending response for testing assurance.
          console.log(trendNamesList);
          res.send(trendNamesList);
        });
    });
  });

module.exports = router;
