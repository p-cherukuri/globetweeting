import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';

@Injectable()
export class TrendsService {

  constructor(private http: Http) { }

  // TODO Get trends from server API
  getAllTrends() {
    return this.http.get('/api/trends')
                    .map(res => res.json());

                    //.catch(this.handleError);
  }

  //  private handleError (error: Response | any) {
  //    // In a real world app, you might use a remote logging infrastructure
  //    let errMsg: string;
  //    if (error instanceof Response) {
  //      const body = error.json() || '';
  //      const err = body.error || JSON.stringify(body);
  //      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
  //    } else {
  //      errMsg = error.message ? error.message : error.toString();
  //    }
  //    console.error(errMsg);
  //    //return Observable.throw(errMsg);
  //  }
}
