var width = 960,
    height = 960;
var projection = d3.geo.orthographic()
    .translate([width / 2, height / 2])
    .scale(width / 2 - 20)
    .clipAngle(90)
    .precision(0.6);
var canvas = d3.select("body").append("canvas")
    .attr("width", width)
    .attr("height", height);
var c = canvas.node().getContext("2d");
var path = d3.geo.path()
    .projection(projection)
    .context(c);
var title = d3.select(".title");
queue()
    .defer(d3.json, "src/app/globe/data/world-110m.json")
    .defer(d3.tsv, "src/app/globe/data/world-country-names.tsv")
    .await(ready);


    // Open a socket to website.
    // function openSock(){
    //   return io({ "force new connection" : true });
    // }
    var socket = io({ "force new connection" : true });
    // Enter a new trend
    function newTerm(){
        //socket.close();
        substring = "";
        substring = document.getElementById("inputSearch").value;
        //socket.open();
        //socket.emit('searchTerm', {inputTerm: input});
        //console.log('Hey');
      }

    function stopConnection(){
      socket.close();
    }

    function openConnectionAgain(){
      socket.open();
    }
      var substring = "trump";
      var otherSubstring = "Trump";
      var united = "united"

function ready(error, world, names) {
  if (error) throw error;
  var globe = {type: "Sphere"},
      land = topojson.feature(world, world.objects.land),
      countries = topojson.feature(world, world.objects.countries).features,
      borders = topojson.mesh(world, world.objects.countries, function(a, b) { return a !== b; });
  countries = countries.filter(function(d) {
    return names.some(function(n) {
      if (d.id == n.id) return d.name = n.name;
    });
  }).sort(function(a, b) {
    return a.name.localeCompare(b.name);
});



// Function to render embedded tweet
// TODO - make this look better.
function makeHTML(tweet) {
  return [
    '<blockquote class="twitter-tweet">',
    '<h1 class="inner-title">', '<img class="twitter-logo-a" src="Twitter_Logo_Blue.png" width="40" height="40">', tweet.placeName, '</h1>',
    '<a href="https://twitter.com/', tweet.user, '/status/', tweet.id, '" target="_blank">',
    tweet.text, '<br />','<br />', '</a>',
      '<a href="https://twitter.com/', tweet.user, '" target="_blank">', '<b>','&mdash; ', tweet.name, ' (', '@', tweet.user,')','</b>', '</a>',
     '</blockquote>',
  ].join('');
}


//tweet.text.includes(substring)==true || tweet.text.includes(otherSubstring)==true
socket.on( 'tweet', function(tweet) {
  if(tweet.text.toLowerCase().indexOf(substring) >= 0) {
    console.log('tweet');
    d3.transition()
        .duration(1250)
        .each("start", function() {
       title.html(makeHTML(tweet));
     })
     .tween("rotate", function() {
       var p = tweet.latLong,
           r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
       return function(t) {
         // Rotate globe so that immediate point is centered in the view.
         projection.rotate(r(t));
         // Erase canvas.
         c.clearRect(0, 0, width, height);

        // Draw a circle at canvas-coordinates of latlong.
         var center = projection(p);
         c.strokeStyle = "#1DA1F3", c.fillStyle = "#fff", c.beginPath(), c.arc(center[0], center[1], 5, 0, 2 * Math.PI, false), c.lineWidth = 8, c.fill(), c.stroke();
         // Draw country borders in white.
         c.strokeStyle = "rgba(4, 148, 185, 1)", c.lineWidth = .5, c.beginPath(), path(borders), c.stroke();
         // Fill in land in green.   #5fc84e
         c.fillStyle = "#5cb85c", c.beginPath(), path(land), c.fill();


         c.globalCompositeOperation = "destination-over";
         // Draw globe circumference in Twitter blue.
         c.strokeStyle = "#1DA1F3", c.lineWidth = 2, c.beginPath(), path(globe), c.stroke();
        // Fill globe with Twitter blue water outside of land borders.
         c.fillStyle = "#1DA1F3", c.beginPath(), path(globe), c.fill();
  //#1b7be9
  //#328df5
  //rgba(4, 148, 185, 1)
  //rgba(57, 162, 190, 0.94)
        };
      });
  }

  });
}

d3.select(self.frameElement).style("height", height + "px");
