import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { Title }     from '@angular/platform-browser';
import { TrendsService } from '../../services/trends.service';


//import * as D3 from 'd3';
//import * as Queue from 'd3-queue';
//import * as TopoJson from 'topojson';
//import io from 'socket.io-client';

import { TdLoadingService, TdDigitsPipe } from '@covalent/core';

//import { ItemsService, UsersService, ProductsService, AlertsService } from '../../services';

//import {MdSlideToggleModule} from '@angular/material';

//import * as world from './data/world-110.json';
//import * as countryNames from './data/world-country-names.tsv';

@Component({
  selector: 'qs-dashboard',
  templateUrl: './globe.component.html',
  styleUrls: ['./globe.component.scss'],
  //viewProviders: [ ItemsService, UsersService, ProductsService, AlertsService ],
})
export class GlobeComponent implements OnInit, AfterViewInit {
    trends: any = [];

    routes: Object[] = [{
        title: 'Globe',
        route: '/',
      }, {
        title: 'About',
        route: '/about',
      },
    ];

    constructor(private elementRef:ElementRef, private trendsService: TrendsService, private _router: Router) {};

    ngOnInit() {
      // Retrieve daily trends from API
      this.trendsService.getAllTrends().subscribe(trends => {
        this.trends = trends;
      });
    }

     ngAfterViewInit(): void {

        var d3Script = document.createElement("script");
        d3Script.type = "text/javascript";
        d3Script.src = "//d3js.org/d3.v3.min.js";
        this.elementRef.nativeElement.appendChild(d3Script);

        var queueScript = document.createElement("script");
        queueScript.type = "text/javascript";
        queueScript.src = "//d3js.org/queue.v1.min.js";
        this.elementRef.nativeElement.appendChild(queueScript);

        var topojsonScript = document.createElement("script");
        topojsonScript.type = "text/javascript";
        topojsonScript.src = "//d3js.org/topojson.v1.min.js";
        this.elementRef.nativeElement.appendChild(topojsonScript);

        var socketioScript = document.createElement("script");
        socketioScript.type = "text/javascript";
        socketioScript.src = "https://cdn.socket.io/socket.io-1.4.5.js";
        this.elementRef.nativeElement.appendChild(socketioScript);

        var planetaryScript = document.createElement("script");
        planetaryScript.type = "text/javascript";
        planetaryScript.src = "assets/worlddata/planetaryjs.min.js";
        this.elementRef.nativeElement.appendChild(planetaryScript);

        var jQueryScript = document.createElement("script");
        jQueryScript.type = "text/javascript";
        jQueryScript.src = "https://code.jquery.com/jquery-3.2.1.min.js";
        jQueryScript.integrity = "sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=";
        jQueryScript.crossOrigin = "anonymous";
        this.elementRef.nativeElement.appendChild(jQueryScript);

        var bootstrapScript = document.createElement("script");
        bootstrapScript.type = "text/javascript";
        bootstrapScript.src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js";
        bootstrapScript.integrity = "sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa";
        bootstrapScript.crossOrigin = "anonymous";
        this.elementRef.nativeElement.appendChild(bootstrapScript);

        var globeScript = document.createElement("script");
        globeScript.type = "text/javascript";
        globeScript.src = "assets/worlddata/globe.js";
        this.elementRef.nativeElement.appendChild(globeScript);

        var googleAnalyticsScript = document.createElement("script");
        googleAnalyticsScript.type = "text/javascript";
        googleAnalyticsScript.src = "assets/worlddata/googleanalytics.js";
        this.elementRef.nativeElement.appendChild(googleAnalyticsScript);
    }

    goToAbout(): void {
      this._router.navigate(['/about']);
    }



}
