import { Component, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements AfterViewInit {


  constructor(private _router: Router, private elementRef:ElementRef) {}

ngAfterViewInit(): void {
  var jQueryScript = document.createElement("script");
  jQueryScript.type = "text/javascript";
  jQueryScript.src = "https://code.jquery.com/jquery-3.2.1.min.js";
  jQueryScript.integrity = "sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=";
  jQueryScript.crossOrigin = "anonymous";
  this.elementRef.nativeElement.appendChild(jQueryScript);

  var menuScript = document.createElement("script");
  menuScript.type = "text/javascript";
  menuScript.src = "assets/worlddata/animateMenu.js";
  this.elementRef.nativeElement.appendChild(menuScript);

  var googleAnalyticsScript = document.createElement("script");
  googleAnalyticsScript.type = "text/javascript";
  googleAnalyticsScript.src = "assets/worlddata/googleanalytics.js";
  this.elementRef.nativeElement.appendChild(googleAnalyticsScript);
}

  goBack(): void {
      this._router.navigate(['/']);
  }
}
