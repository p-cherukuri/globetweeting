/**
 * Server functions:
 * Opens real-time data stream from Twitter client to receive tweets.
 * Posts all tweets with geotags to a Socket.io socket.
 */

/**
 * Express.js setup
 */
const STREAM_URL = 'statuses/filter';
const OEMBED_URL = 'statuses/oembed';
const TRENDS_URL = 'trends/place';
const MAX_WIDTH = 305;
const portNum = process.env.PORT || '3000';

var usa_query = "";
var usa_trends = [];

const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const dbInfo = require('./config/db');
// Get our API routes
const api = require('./server/routes/api');
const app = express();
// POST data parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Static files served in 'dist' directory
app.use(express.static(path.join(__dirname, '/dist')));

//Set API routes
app.use('/api', api);

// Serving 'index.html'
app.get('*', function(req, res){
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});
// Store port number in Express
app.set('port', portNum)
// Create HTTP Server
//const server = http.createServer(app);
const server = http.Server(app);
// Listen on the provided port number, on all network interfaces
server.listen(portNum, () => console.log('Server running on localhost:$PORT'));




/**
 * Decouple Twitter listener and Socket.io socket by creating EventEmitter
 *
 */
var EventEmitter = require('events'),
    util = require('util');

function TweetEmitter() {
  EventEmitter.call(this);
}
util.inherits(TweetEmitter, EventEmitter);

var tweetEmitter = new TweetEmitter();

/**
 * Socket.io setup
 */

var io = require('socket.io')(server);
var query = 'trump';                          // user search
tweetEmitter.on('tweet', function(tweet) {
  console.log(tweet);
  io.emit('tweet', tweet);
});
// user search
io.sockets.on('connection', function(socket){
    socket.on('searchTerm', function (msg) {
        query = msg.inputTerm;
        //console.log(msg.inputTerm);
        newTweets();
    })
});

// a helper function to average coordinate pairs
function average(coordinates) {
  var n = 0, lon = 0.0, lat = 0.0;
  coordinates.forEach(function(latLongs) {
    latLongs.forEach(function(latLong) {
      lon += latLong[0];
      lat += latLong[1];
      n += 1;
    })
  });
  return [lon / n, lat / n];
}
// Parse trends received from Twitter API from json to CSV
function parseTrends (trends) {
  //let query = "";
  usa_trends = [];
  usa_query = "";

  usa_trends = trends;
  trends.forEach((trend) => {
    usa_query = usa_query.concat(trend.name, ',');
    //TODO if tweetVolume
    //usa_trends.push(trend.name)
  });
  usa_query = usa_query.concat('Trump');
  console.log(usa_query);
  //return query;
}

function getOEmbed (tweet) {

    // oEmbed request params
    var params = {
      "id": tweet.id_str,
      "maxwidth": MAX_WIDTH,
      "hide_thread": true,
      "omit_script": true
    };

    // request data
    twitter.get(OEMBED_URL, params, function (err, data, resp) {
      tweet.oEmbed = data;
      var oEmbedTweet = tweet;

      // do we have oEmbed HTML for all Tweets?
      if (tweet.oEmbed) {
        return tweet.oEmbed;
      }
    });
  }

// Twitter client setup

var Twitter = require('twitter'),
    credentials = require('./config/credentials.js'),
    client = new Twitter(credentials);
// Twitter keyword to filter tweets entered here
// TODO - add a way for user to enter this search keyword themselves
//var query = process.argv[2] || 'trump';
// USA WOEID - 23424977
function newTrends(callback) {
  //var trendsQuery = ""
  client.get(TRENDS_URL, {id: 23424977}, function(err, data) {
      callback(data[0].trends);
    // trendsQuery = parseTrends(data[0].trends);
    // console.log(trendsQuery);

  });
  // console.log(trendsQuery);
  // return trendsQuery;
}


// usa_trends = client.trends.place(23424977);
//
// let usa_query = "";
//
// usa_query = parseTrends(usa_trends);
//
// var _stream = {};

newTrends((trends) => {
    parseTrends(trends);
});

// After daily trends are retrieved and parsed from Twitter client,
setTimeout(() => {
  //console.log(usa_query);

  MongoClient.connect(dbInfo.url, (err, database) => {
    if (err) {return console.log(err);}

    //require('./server/routes/api')(app, database);
    database.collection('trends').remove( {} );
    database.collection('trends').insert(usa_trends, (err, result) => {
      if (err) {
        console.log('An error has occurred. Twitter trends could not be retrieved.');
      } else {
        console.log('Twitter trends retrieved!');
      }
    });
  })



  newTweetsStream();
}, 5000);
// Call trends API and retrieve top trends every 5 minutes
setInterval(() => {

  newTrends((trends) => {
      parseTrends(trends);
  });

  MongoClient.connect(dbInfo.url, (err, database) => {
    if (err) {return console.log(err);}

    database.collection('trends').remove( {} );
    database.collection('trends').insert(usa_trends, (err, result) => {
      if (err) {
        console.log('An error has occurred. Twitter trends could not be updated.');
      } else {
        console.log('Twitter trends updated!');
      }
    });
  })
}, 305000);

function newTweets() {
    console.log('User chose new search topic: ' + query);
    _stream.destroy();
    newTweetsStream();
}

function newTweetsStream() {
  client.stream(STREAM_URL, {track: usa_query}, function(stream) {
    _stream = stream;
    // Every time a tweet is received
    stream.on('data', function(tweet) {
      // And the tweet has 'place' field populated
      if (tweet.place) {
        // Extract necessary fields from client
        if (tweet.place.bounding_box.coordinates != null) {
          var tweetSmall = {
            id: tweet.id_str,
            user: tweet.user.screen_name,
            name: tweet.user.name,
            text: tweet.text,
            placeName: tweet.place.full_name,
            latLong: average(tweet.place.bounding_box.coordinates),
            date: tweet.created_at,
          }

          tweetEmitter.emit('tweet', tweetSmall);
        }


      /*  if ( tweet.entities['media'] ) {
          if ( tweet.entities['media'][0].type == "photo" ) {
            tweetSmall.mediaUrl = tweet.entities['media'][0].media_url;
          } else {
            tweetSmall.mediaUrl = null;
          }
        } */
        //var embeddedTweet = getOEmbed(tweet);
        // Notify tweetEmitter

      }
    });
  });
}
